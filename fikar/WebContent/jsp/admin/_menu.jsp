<%@ taglib prefix="s" uri="/struts-tags" %>

<ul class="nav nav-sidebar">
	
	
	
 	<li><a href="<s:url action= 'etudiantCreate'	/>" >	<s:text name= "global.menu.admin.etudiant.create"	/></a></li>
 	<li><a href="<s:url action= 'etudiantList'		/>" >	<s:text name= "global.menu.admin.etudiant.list"		/></a></li>
 	
 	
 	
 	<li><a href="<s:url action= 'enseignantCreate'	/>" >	<s:text name= "global.menu.admin.enseignant.create"	/></a></li>
 	<li><a href="<s:url action= 'enseignantList'	/>" >	<s:text name= "global.menu.admin.enseignant.list"	/></a></li>
 	
 	
 	
 	<li><a href="<s:url action= 'adminCreate'		/>" >	<s:text name= "global.menu.admin.admin.create"		/></a></li>
 	<li><a href="<s:url action= 'adminList'			/>" >	<s:text name= "global.menu.admin.admin.list"		/></a></li>
 	
 	
 	
 	<li><a href="<s:url action= 'entrepriseCreate'	/>" >	<s:text name= "global.menu.admin.entreprise.create"	/></a></li>
 	<li><a href="<s:url action= 'entrepriseList'	/>" >	<s:text name= "global.menu.admin.entreprise.list"	/></a></li>
 	
 	
 	
 	<li><a href="<s:url action= 'conventionCreate'	/>" >	<s:text name= "global.menu.admin.convention.create"	/></a></li>
 	<li><a href="<s:url action= 'conventionList'	/>" >	<s:text name= "global.menu.admin.convention.list"	/></a></li>
 	
 	
 	
 	<li><a href="<s:url action= 'offreValidationList'	/>" >	<s:text name= "global.menu.admin.offre.validation.list"	/></a></li>
 	
 	
 	
 	
 	<li><a href="<s:url action= 'logOut'	/>" >	<s:text name= "global.menu.logOut"	/></a></li>
</ul>
