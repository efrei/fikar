<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<s:actionmessage 	theme = "bootstrap" />

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.admin.create" /></b></h3>
	</div>

	<div class="panel-body">
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
					<s:actionerror/>
					<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "adminCreation.execute"			method	= "post"
					validate= "true" 							theme	= "bootstrap"
					cssClass= "form-horizontal default-form"	id		= "validationForm" 
		>
			
		<div class="row">
			
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8"> 
					<s:textfield cssClass="form-control"
								requiredLabel="true"
								key="form.label.nom"
								name="admin.per.nom" 
				/></div>
				<div class="form-group field-resize" label-length="3" field-length="8"> <s:textfield cssClass="form-control"
								requiredLabel="true"
								key="form.label.prenom" 
								name="admin.per.prenom"
				/></div>
			</div>
			
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8"> <s:textfield cssClass="form-control"
								requiredLabel="true"
								key="form.label.email"
								name="admin.per.email"
				/></div>
				<div class="form-group  field-resize" label-length="3" field-length="8">
					 <s:textfield cssClass="form-control"
								requiredLabel="false"
								key="form.label.tel"
								name="admin.per.tel" 	/>
				</div>
			</div>
			
		</div>
			
		<div class="row form-btn-pad">
			<div class="col-lg-2 col-lg-offset-5" >
				<s:submit 	key				= "global.button.create"
							validate		= "true"
							cssClass		= "btn btn-primary btn-sm btn-block"
							validateFunction= "bootstrapValidation"
							formIds			= "validationForm"		
				/>
			</div>
		</div>
			
		</s:form>
	</div>
</div>