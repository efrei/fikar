<%@ taglib prefix="s" 	uri="/struts-tags" %>

<s:if test="admin.id">
	<s:set name="action" value="'adminUpdate'"/>
	<s:set name="title" value="'page.admin.title.admin.update'"/>
	<s:set name="submitButton" value="%{getText('global.button.update')}"/>
	<s:set name="emailDisabled" value="true"/>
	<s:set name="email" value="admin.per.email"/>
	<s:set name="myIdBean" value="idBean"/>
</s:if>
<s:else>
	<s:set name="action" value="'adminSave'"/>
	<s:set name="title" value="'page.admin.title.admin.create'"/>
	<s:set name="submitButton" value="%{getText('global.button.create')}"/>
	<s:set name="emailDisabled" value="false"/>
</s:else>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="%{#title}" /></b></h3>
	</div>

	<div class="panel-body">
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
					<s:actionerror/>
					<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "%{#action}"						method	= "post"
					validate= "true" 							theme	= "bootstrap"
					cssClass= "form-horizontal default-form"	id		= "validationForm" 
		>
		
		<s:if test="admin.id">
			<s:hidden name="idBean" value="%{#myIdBean}" />
			<s:hidden name="admin.per.email" value="%{#email}" />
		</s:if>
		
		<div class="row">
			
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8"> 
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.nom"
									name="admin.per.nom" 
				/></div>
				<div class="form-group field-resize" label-length="3" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.prenom" 
									name="admin.per.prenom"
				/></div>
			</div>
			
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.email"
									name="admin.per.email"
									disabled="%{#emailDisabled}"
				/></div>
				<div class="form-group  field-resize" label-length="3" field-length="8">
					 <s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.tel"
									name="admin.per.tel" 	/>
				</div>
			</div>
			
		</div>
			
		<div class="row form-btn-pad">
			<div class="col-lg-2 col-lg-offset-5" >
				<s:submit 	key				= "%{#submitButton}"
							validate		= "true"
							cssClass		= "btn btn-primary btn-sm btn-block"
							validateFunction= "bootstrapValidation"
							formIds			= "validationForm"		
				/>
			</div>
		</div>
			
		</s:form>
	</div>
</div>