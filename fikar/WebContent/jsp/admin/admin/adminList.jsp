<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.admin.search" /></b></h3>
	</div>
	
	<div class="panel-body">
		
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
					<s:actionerror/>
					<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "adminList"						method	= "post"
					validate= "true" 							theme	= "bootstrap"
					cssClass= "form-horizontal default-form"	id		= "validationForm" 
		>
			
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield	cssClass="form-control"
										key="form.label.nom"
										name="nom" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield	cssClass="form-control"
										key="form.label.prenom"
										name="prenom" />
					</div>
				</div>
			</div>
			
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<s:submit 	key				= "global.button.search"
								validate		= "true"
								cssClass		= "btn btn-primary btn-sm btn-block"
								validateFunction= "bootstrapValidation"
								formIds			= "validationForm"		
					/>
				</div>
			</div>
			
		</s:form>
		
	</div>
	
</div>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.admin.list" /></b></h3>
	</div>
	
	<div class="panel-body">
		<div class="panel-body">
	    	<div class="table-responsive" style="overflow-x:scroll;">
	        	<table  class="table table-striped table-bordered table-hover" >
	        	
	        		<thead>
	        			<tr>
	        				<th><s:text name="form.label.nom" /></th>
							<th><s:text name="form.label.prenom" /></th>
							<th><s:text name="form.label.email" /></th>
							<th><s:text name="form.label.tel" /></th>
							<th>
								<div class='text-center'><span class="inverse glyphicon glyphicon-th"></span></div>
							</th>
	        			</tr>
	        		</thead>
	        		
	        		<tbody>
	        			<s:iterator value="adminList">
		        			<tr>
		        				<td><s:property value="per.nom"/></td>
								<td><s:property value="per.prenom"/></td>
								<td><s:property value="per.email"/></td>
								<td><s:property value="per.tel"/></td>
								<td> 
									<a class="pull-left" href="<s:url action= 'adminShow'><s:param name='idBean' value='id'/></s:url> " >
										<span class="glyphicon glyphicon-search" ></span>
									</a>
									<a class="pull-right" href="<s:url action= 'adminModify'><s:param name='idBean' value='id'/></s:url>" >
										<span class="glyphicon glyphicon-pencil" ></span>
									</a>
								</td>
		        			</tr>
	        			</s:iterator>
	        		</tbody>
	        	
	        	</table>
	        </div>
	    </div>
	</div>
	
</div>