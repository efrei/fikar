<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.etudiant.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="admin.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="admin.per.prenom"/></dd>
				</dl>
			</div>
			
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="admin.per.email"/></dd>
				</dl>
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="admin.per.tel"/></dd>
				</dl>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-5" >
				<a href="<s:url action= 'adminModify'><s:param name='idBean' value='idBean'/></s:url>" >
					<button type="button" class="btn btn-primary btn-sm btn-block">
						<s:text name="global.button.update"/>
					</button>
				</a>
			</div>
		</div>
		
	</div>
</div>