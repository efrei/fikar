<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Informations de base</b></h3>
	</div>
	
	<div class="panel-body">
	
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group field-resize" label-length="2" field-length="10">
					<s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.titre"
									name="conv.titre" 	/>
				</div>
				<div class="form-group field-resize" label-length="2" field-length="10">
					<s:textarea 	cssClass="form-control" 
									key="form.label.descrip"
									name="conv.descrip" />
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									type="date"
									id="datepicker"
									key="form.label.dateDebut" 
									name="dateDString"/>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									type="date"
									id="datepicker"
									key="form.label.dateF" 
									name="dateFString"/>
				</div>
			</div>
		</div>
	</div>
</div>