<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Tuteur pédagogique</b></h3>
	</div>
	
	<div class="panel-body">

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.matricule"
									name="ensInput.matricule"
									disabled="true"	/>
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield	cssClass="form-control"
									requiredLabel="true"
									key="form.label.nom"
									name="ensInput.per.nom" 
									disabled="true"	/>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.email"
									name="ensInput.per.email"
									disabled="true"	/>
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.prenom" 
									name="ensInput.per.prenom"
									disabled="true"	/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-10" >
				<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#ensModal" onclick="cleanEnsModal();">
					<s:text name="global.button.search"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ensModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<jsp:include page="../../enseignant/_ajax/enseignantListModalAjax.jsp" />
			
		</div>
	</div>
</div>

<script>
	function enseignantSelect(id,nom,prenom,email,matricule) {
		$('#idEns').val(id);
		$("input[name='ensInput.per.nom']").val(nom);
		$("input[name='ensInput.per.prenom']").val(prenom);
		$("input[name='ensInput.per.email']").val(email);
		$("input[name='ensInput.matricule']").val(matricule);
	}
	
	function cleanEnsModal() {
 		$('#ensAjaxListResult').html('');
		$("input[name='nom']").val('');
		$("input[name='prenom']").val('');
		$("input[name='email']").val('');
		$("input[name='matricule']").val(''); 
	}
</script>