<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Entreprise</b></h3>
	</div>
	
	<div class="panel-body">

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.rcs"
									name="entrepInput.rcs" 
									disabled="true	"/>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.siret"
									name="entrepInput.siret" 	
									disabled="true"	/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group field-resize" label-length="2" field-length="10">
					<s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.raisonsocial"
									name="entrepInput.raisonSociale" 	
									disabled="true"	/>
				</div>
			</div>
		</div>	
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-10" >
				<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#entrepModal" onclick="cleanEntrepModal();">
					<s:text name="global.button.search"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="entrepModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<jsp:include page="../../entreprise/_ajax/entrepriseListModalAjax.jsp" />
			
		</div>
	</div>
</div>

<script>
	function entrepriseSelect(id,rcs,siret,raisonSociale) {
		$('#idEntrep').val(id);
		$("input[name='entrepInput.rcs']").val(rcs);
		$("input[name='entrepInput.siret']").val(siret);
		$("input[name='entrepInput.raisonSociale']").val(raisonSociale);
	}
	function cleanEntrepModal()	{
		$('#entrepAjaxListResult').html('');
		$("input[name='siret']").val('');
		$("input[name='rcs']").val('');
		$("input[name='raisonSociale']").val('');
	}
</script>