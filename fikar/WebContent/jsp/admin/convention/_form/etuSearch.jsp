<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Etudiant</b></h3>
	</div>
	
	<div class="panel-body">

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.ine"
									name="etuInput.ine" 
									disabled="true"	/>
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield	cssClass="form-control"
									requiredLabel="true"
									key="form.label.nom"
									name="etuInput.per.nom"
									disabled="true"	/>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="false"
									key="form.label.numEtu"
									name="etuInput.num" 	
									disabled="true"	/>
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.prenom" 
									name="etuInput.per.prenom" 
									disabled="true"	/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-10" >
				<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#etuModal" onclick="cleanEtuModal();">
					<s:text name="global.button.search"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="etuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<jsp:include page="../../etudiant/_ajax/etudiantListModalAjax.jsp" />
			
		</div>
	</div>
</div>

<script>
	function etudiantSelect(id,nom,prenom,ine,num) {
		$('#idEtu').val(id);
		$("input[name='etuInput.per.nom']").val(nom);
		$("input[name='etuInput.per.prenom']").val(prenom);
		$("input[name='etuInput.ine']").val(ine);
		$("input[name='etuInput.num']").val(num);
	}
	function cleanEtuModal() {
		$('#etuAjaxListResult').html('');
		$("input[name='nom']").val('');
		$("input[name='prenom']").val('');
		$("input[name='ine']").val('');
		$("input[name='num']").val('');
	}
</script>