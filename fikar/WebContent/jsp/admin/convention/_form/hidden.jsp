<%@ taglib prefix="s" 	uri="/struts-tags" %>

<s:hidden name="idEtu" 		id="idEtu"		value="" />
<s:hidden name="idEntrep" 	id="idEntrep"	value="" />
<s:hidden name="idEns" 		id="idEns"		value="" />

<s:hidden name="etuInput.ine" 			value=""/>
<s:hidden name="etuInput.num" 			value=""/>
<s:hidden name="etuInput.per.nom" 		value=""/>
<s:hidden name="etuInput.per.prenom" 	value=""/>

<s:hidden name="entrepInput.rcs" 			value=""/>
<s:hidden name="entrepInput.siret" 			value=""/>
<s:hidden name="entrepInput.raisonSociale" 	value=""/>

<s:hidden name="ensInput.per.nom" 		value=""/>
<s:hidden name="ensInput.per.prenom" 	value=""/>
<s:hidden name="ensInput.per.email" 	value=""/>
<s:hidden name="ensInput.matricule" 	value=""/>