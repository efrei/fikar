<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Ma�tre de stage</b></h3>
	</div>
	
	<div class="panel-body">
	
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield	cssClass="form-control"
									requiredLabel="true"
									key="form.label.nom"
									name="conv.nomMaitre" />
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.email" 
									name="conv.mailMaitre" />
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.prenom" 
									name="conv.prenomMaitre" />
				</div>
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.tel" 
									name="conv.telMaitre" />
				</div>
			</div>
		</div>
		
	</div>
	
</div>