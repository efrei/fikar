<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>Saisie de convention</b></h3>
	</div>
	
	<div class="panel-body">
		
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
				<s:actionerror/>
				<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "conventionSave"									method	= "post"
					validate= "true" 								theme	= "bootstrap"
					cssClass= "form-horizontal default-form"		id		= "validationForm" 
		>
			<jsp:include page="_form/hidden.jsp"	 	/>
		<br/>
			<jsp:include page="_form/convFields.jsp"	/>
		<br/>
			<jsp:include page="_form/etuSearch.jsp" 	/>
		<br/>
			<jsp:include page="_form/entrepSearch.jsp" 	/>
		<br/>
			<jsp:include page="_form/ensSearch.jsp" 	/>
		<br/>
			<jsp:include page="_form/maitre.jsp"	 	/>
		<br/>
			
		<div class="row form-btn-pad">
			<div class="col-lg-2 col-lg-offset-5" >
				<s:submit 	key				= "Create"
							validate		= "true"
							cssClass		= "btn btn-primary btn-sm btn-block"
							validateFunction= "bootstrapValidation"
							formIds			= "validationForm"		
				/>
			</div>
		</div>
		
		<br/>
		
		</s:form>
		
	</div>
	
</div>
