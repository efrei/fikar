<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.convention.search" /></b></h3>
	</div>
	
	<div class="panel-body">
	
		<div class="panel panel-primary">
	
		<div class="panel-heading">
			<h3 class="panel-title"><b><s:text name="page.admin.title.convention.information" /></b></h3>
		</div>
		<div class="panel-body">
			<div class="row">		
				<div class="col-lg-12">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.titre"/></dt>
						<dd><s:property value="conv.titre"/></dd>
					</dl>
				</div>
			</div>		
			<div class="row">		
				<div class="col-lg-12">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.descrip"/></dt>
						<dd><s:property value="conv.descrip"/></dd>
					</dl>
				</div>
			</div>
			<div class="row">		
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.dateD"/></dt>
						<dd><s:property value="dateDString"/></dd>
					</dl>
				</div>
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.dateF"/></dt>
						<dd><s:property value="dateFString"/></dd>
					</dl>
				</div>
			</div>
		</div>
		</div>
	
	<div class="panel-body">
	
	
	
	<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.etudiant.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
		
			<div class="col-lg-6">
			
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="conv.etu.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="conv.etu.per.prenom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="conv.etu.per.email"/></dd>
				</dl>
				
			</div>
			
			<div class="col-lg-6">
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.dateN"/></dt>
					<dd><s:property value="dateStringN" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="conv.etu.per.tel" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.ine"/></dt>
					<dd><s:property value="conv.etu.ine" default="Non renseigné"/></dd>
				</dl>
				
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.grp"/></dt>
					<dd><s:property value="conv.etu.grp.nom"/></dd>
				</dl>
			</div>
			
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.numEtu"/></dt>
					<dd><s:property value="conv.etu.num" default="Non renseigné"/></dd>
				</dl>
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-12">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.adr.title"/></dt>
					<dd><s:property value="conv.etu.adr.complement"/></dd>
					<dd><s:property value="conv.etu.adr.num"/>, <s:property value="conv.etu.adr.rue"/></dd>
					<dd><s:property value="conv.etu.adr.cp"/> <s:property value="conv.etu.adr.ville"/></dd>
					<dd><s:property value="conv.etu.adr.pays"/></dd>
				</dl>
			</div>
			
		</div>
	</div>
</div>




<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.entreprise.show" /></b></h3>
	</div>
	
	<div class="panel-body">
		
		<div class="row">
		
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.raisonsocial"/></dt>
					<dd><s:property value="conv.entrep.raisonSociale"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.siret"/></dt>
					<dd><s:property value="conv.entrep.siret"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.rcs"/></dt>
					<dd><s:property value="conv.entrep.rcs"/></dd>
				</dl>
			</div>
			
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.adr.title"/></dt>
					<dd><s:property value="conv.entrep.adr.complement"/></dd>
					<dd><s:property value="conv.entrep.adr.num"/>, <s:property value="conv.entrep.adr.rue"/></dd>
					<dd><s:property value="conv.entrep.adr.cp"/> <s:property value="conv.entrep.adr.ville"/></dd>
					<dd><s:property value="conv.entrep.adr.pays"/></dd>
				</dl>
			</div>
			
		</div>
	</div>
	
</div>








		<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.enseignant.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
		
			<div class="col-lg-6">
			
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="conv.tuteur.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="conv.tuteur.per.prenom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="conv.tuteur.per.email"/></dd>
				</dl>
				
			</div>
			
			<div class="col-lg-6">
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="conv.tuteur.per.tel" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.matricule"/></dt>
					<dd><s:property value="conv.tuteur.matricule" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.fonctions"/></dt>
					<dd><s:property value="conv.tuteur.fonctions" default="Non renseigné"/></dd>
				</dl>
				
			</div>
		
		</div>

	</div>
</div>
	
	<div class="panel-body">
	
		<div class="panel panel-primary">
	
		<div class="panel-heading">
			<h3 class="panel-title"><b><s:text name="page.admin.title.convention.maitre" /></b></h3>
		</div>
		<div class="panel-body">
			<div class="row">		
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.nom"/></dt>
						<dd><s:property value="conv.nomMaitre"/></dd>
					</dl>
				</div>		
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.email"/></dt>
						<dd><s:property value="conv.mailMaitre"/></dd>
					</dl>
				</div>
			</div>
			<div class="row">		
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.prenom"/></dt>
						<dd><s:property value="conv.prenomMaitre"/></dd>
					</dl>
				</div>
				<div class="col-lg-6">
					<dl class="dl-horizontal">
						<dt><s:text name="form.label.tel"/></dt>
						<dd><s:property value="conv.telMaitre"/></dd>
					</dl>
				</div>
			</div>
		</div>
		</div>
	
	
		
	</div>

</div>