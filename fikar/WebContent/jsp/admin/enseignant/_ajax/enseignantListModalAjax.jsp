<%@ taglib prefix="s" 	uri="/struts-tags" %>

<script>

	function searchEnseignant() {

		$.post( 'enseignantListAjax.action', 
				{	'nom'		: $('#nom').val(),
				 	'prenom'	: $('#prenom').val(),
				 	'email'		: $('#email').val(),
				 	'matricule'		: $('#matricule').val(),
				},
				function(data) {
					$('#ensAjaxListResult').html(data);
				}
		);
		
	}
	
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title"><s:text name="page.admin.title.enseignant.search" /></h4>
</div>
			
<div class="modal-body">
	<div class="panel panel-primary">
		
		<div class="panel-heading">
			<h3 class="panel-title"><b><s:text name="page.admin.title.enseignant.search" /></b></h3>
		</div>
	
		<div class="panel-body">
				
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield	cssClass="form-control"
										key="form.label.nom"
										id="nom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield 	cssClass="form-control"
										key="form.label.email"
										id="email" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield 	cssClass="form-control"
										key="form.label.prenom" 
										id="prenom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="7">
						<s:textfield 	cssClass="form-control"
										key="form.label.matricule"
										id="matricule" />
					</div>
				</div>
			</div>
		
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<input value="Rechercher" type="button" class="btn btn-primary btn-sm btn-block" onclick="searchEnseignant();" />
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="panel panel-primary">
		
		<div class="panel-heading">
	    	<h3 class="panel-title"><b><s:text name="page.admin.title.enseignant.list" /></b></h3>
		</div>
		
		<div class="panel-body">
			<div id="ensAjaxListResult">
				<!-- Result of the Ajax script on the top -->
			</div>
		</div>
		
	</div>
</div>
			
<div class="modal-footer">
	<div class="row">
		<div class="col-lg-3 col-lg-offset-9">
			<button type="button" class="btn btn-default btn-sm btn-block" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>