<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="table-responsive" style="overflow-x:scroll;">
	
	<table  class="table table-striped table-bordered table-hover" >

		<thead> 
			<tr>
				<th>
					<div class='text-center'><span class="inverse glyphicon glyphicon-th"></span></div>
				</th>
				<th><s:text name="form.label.nom" /></th>
				<th><s:text name="form.label.prenom" /></th>
				<th><s:text name="form.label.email" /></th>
				<th><s:text name="form.label.matricule" /></th>
			</tr>
		</thead>

	<tbody>
		<s:iterator value="ensList">
			<tr>
				<td> 
					<a 	class="pull-left"  href="#" data-dismiss="modal"
						onclick="enseignantSelect(	'<s:property value="id"/>',
													'<s:property value="per.nom"/>',
													'<s:property value="per.prenom"/>',
													'<s:property value="per.email"/>',
													'<s:property value="matricule"/>'
												);"
					>
						<span class="glyphicon glyphicon-tag" ></span>
					</a>
				</td>
				<td><s:property value="per.nom"/></td>
				<td><s:property value="per.prenom"/></td>
				<td><s:property value="per.email"/></td>
				<td><s:property value="matricule"/></td>
			</tr>
		</s:iterator>
	</tbody>
		
	</table>
</div>