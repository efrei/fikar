<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<%-- <s:set name="action" value="actionForm"/> --%>
<s:if test="enseignant.id">
	<s:set name="action" value="'enseignantUpdate'"/>
</s:if>
<s:else>
	<s:set name="action" value="'enseignantSave'"/>
</s:else>

	<s:set name="myIdBean" value="idBean"/>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b>
			<s:if test="enseignant.id">
				<s:set name="action"  value="'enseignantUpdate'"/>
				<s:set name="disable" value="'disable'"/>
				<s:text name="page.admin.title.enseignant.update" />
			</s:if>
			<s:else>
				<s:set name="action" value="'enseignantSave'"/>
				<s:set name="disable" value="''"/>
				<s:text name="page.admin.title.enseignant.create" />
			</s:else>
		</b></h3>
	</div>

	<div class="panel-body">
		
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
					<s:actionerror/>
					<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "%{#action}"							method	= "post"
					validate= "true" 								theme	= "bootstrap"
					cssClass= "form-horizontal default-form"		id		= "validationForm" 
		>
		
		<s:hidden name="idBean" value="%{#myIdBean}"></s:hidden>	
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield	cssClass="form-control"
										requiredLabel="true"
										key="form.label.nom"
										name="enseignant.per.nom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.prenom" 
										name="enseignant.per.prenom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control "
										requiredLabel="true"
										key="form.label.email"
										name="enseignant.per.email"/>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="false"
										key="form.label.tel"
										name="enseignant.per.tel" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.matricule"
										name="enseignant.matricule" />
					</div>
						<div class="form-group field-resize" label-length="4" field-length="8">
								<s:textarea 	cssClass="form-control" 
												key="form.label.fonctions"
												name="enseignant.fonctions" />
						</div>
				</div>
				
			</div>
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<s:submit 	key				= "global.button.create"
								validate		= "true"
								cssClass		= "btn btn-primary btn-sm btn-block"
								validateFunction= "bootstrapValidation"
								formIds			= "validationForm"		
					/>
				</div>
			</div>
			
		</s:form>
		
	</div>
	
</div>