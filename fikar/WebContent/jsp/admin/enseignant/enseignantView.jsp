<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<s:actionmessage 	theme = "bootstrap" />

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.enseignant.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
		
			<div class="col-lg-6">
			
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="enseignant.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="enseignant.per.prenom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="enseignant.per.email"/></dd>
				</dl>
				
			</div>
			
			<div class="col-lg-6">
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="enseignant.per.tel" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.matricule"/></dt>
					<dd><s:property value="enseignant.matricule" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.fonctions"/></dt>
					<dd><s:property value="enseignant.fonctions" default="Non renseigné"/></dd>
				</dl>
				
			</div>
		
		</div>
		<div class="row">
			<div class="col-lg-2 col-lg-offset-5" >
				<a href="<s:url action= 'enseignantModify'><s:param name='idBean' value='id'/></s:url>" >
					<button type="button" class="btn btn-primary btn-sm btn-block">
						<s:text name="global.button.update"/>
					</button>
				</a>
			</div>
		</div>

	</div>
</div>