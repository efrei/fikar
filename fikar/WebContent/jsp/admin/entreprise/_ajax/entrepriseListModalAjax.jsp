<%@ taglib prefix="s" 	uri="/struts-tags" %>

<script>

	function searchEntreprise() {

		$.post( 'entrepriseListAjax.action', 
				{	'siret'			: $('#siret').val(),
				 	'rcs'			: $('#rcs').val(),
				 	'raisonSociale'	: $('#raisonSociale').val(),
				},
				function(data) {
					$('#entrepAjaxListResult').html(data);
				}
		);
		
	}
	
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title"><s:text name="page.admin.title.entreprise.search" /></h4>
</div>
			
<div class="modal-body">
	<div class="panel panel-primary">
		
		<div class="panel-heading">
			<h3 class="panel-title"><b><s:text name="page.admin.title.entreprise.search" /></b></h3>
		</div>
	
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield	cssClass="form-control"
										key="form.label.siret"
										name="siret" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield	cssClass="form-control"
										key="form.label.rcs"
										name="rcs" />
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group field-resize" label-length="2" field-length="10">
						<s:textfield	cssClass="form-control"
										key="form.label.raisonsocial"
										name="raisonSociale" />
					</div>
				</div>
			</div>
							
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<input value="Rechercher" type="button" class="btn btn-primary btn-sm btn-block" onclick="searchEntreprise();" />
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="panel panel-primary">
		
		<div class="panel-heading">
	    	<h3 class="panel-title"><b><s:text name="page.admin.title.entreprise.list" /></b></h3>
		</div>
		
		<div class="panel-body">
			<div id="entrepAjaxListResult">
				<!-- Result of the Ajax script on the top -->
			</div>
		</div>
		
	</div>
</div>
			
<div class="modal-footer">
	<div class="row">
		<div class="col-lg-3 col-lg-offset-9">
			<button type="button" class="btn btn-default btn-sm btn-block" data-dismiss="modal">Annuler</button>
		</div>
	</div>
</div>