<%@ taglib prefix="s" 	uri="/struts-tags" %>

<div class="table-responsive" style="overflow-x:scroll;">
       	<table  class="table table-striped table-bordered table-hover" >
       	
       		<thead>
       			<tr>
       				<th>
						<div class='text-center'><span class="inverse glyphicon glyphicon-th"></span></div>
					</th>
       				<th><s:text name="form.label.raisonsocial" /></th>
       				<th><s:text name="form.label.siret" /></th>
					<th><s:text name="form.label.rcs" /></th>
       			</tr>
       		</thead>
       		
       		<tbody>
       			<s:iterator value="entrepList">
        			<tr>
        				<td> 
							<a class="pull-left" href="#" data-dismiss="modal"
								onclick="entrepriseSelect(	'<s:property value="id"/>',
															'<s:property value="rcs"/>',
															'<s:property value="siret"/>',
															'<s:property value="raisonSociale"/>'
														);"
							>
								<span class="glyphicon glyphicon-tag" ></span>
							</a>
						</td>
        				<td><s:property value="raisonSociale"/></td>
        				<td><s:property value="siret"/></td>
						<td><s:property value="rcs"/></td>
        			</tr>
       			</s:iterator>
       		</tbody>
       	
       	</table>
       </div>