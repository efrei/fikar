<%@ taglib prefix="s" uri="/struts-tags" %>

<s:if test="entrep.id">
	<s:set name="action" value="'entrepriseUpdate'"/>
	<s:set name="title" value="'page.admin.title.entreprise.update'"/>
	<s:set name="submitButton" value="%{getText('global.button.update')}"/>
	<s:set name="myIdBean" value="idBean"/>
</s:if>
<s:else>
	<s:set name="action" value="'entrepriseSave'"/>
	<s:set name="title" value="'page.admin.title.entreprise.create'"/>
	<s:set name="submitButton" value="%{getText('global.button.create')}"/>
</s:else>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="%{#title}" /></b></h3>
	</div>
	
	<div class="panel-body">
	
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
				<s:actionerror/>
				<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "%{#action}"						method	= "post"
					validate= "true" 							theme	= "bootstrap"
					cssClass= "form-horizontal default-form"	id		= "validationForm" 
		>
		
		<s:if test="entrep.id">
			<s:hidden name="idBean" value="%{#myIdBean}" />
		</s:if>
		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8"> 
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.siret"
									name="entrep.siret" 
				/></div>
			</div>
			<div class="col-lg-6">
				<div class="form-group field-resize" label-length="3" field-length="8"> 
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.rcs"
									name="entrep.rcs" 
				/></div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group field-resize" label-length="3" field-length="8"> 
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.raisonsocial"
									name="entrep.raisonSociale" 
				/></div>
			</div>
		</div>
		
		<hr/>
		
		<div class="row">
				<div class="col-lg-12">
					<div class="form-group field-resize" label-length="2" field-length="10">
						<s:textfield 	cssClass="form-control"
										requiredLabel="false"
										key="form.label.adr.complement"
										name="entrep.adr.complement"
					/></div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="6" field-length="6">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.adr.num"
										name="entrep.adr.num"
					/></div>
				</div>
				<div class="col-lg-8">
					<div class="form-group field-resize" label-length="2" field-length="10">
						<s:textfield 	cssClass="form-control"
							
										requiredLabel="true"
										key="form.label.adr.rue"
										name="entrep.adr.rue" />
					</div>
				</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group field-resize" label-length="6" field-length="6">
					<s:textfield 	cssClass="form-control"
									requiredLabel="true"
									key="form.label.adr.ville"
									name="entrep.adr.ville"/></div>
				</div>
			<div class="col-lg-4">
				<div class="form-group field-resize" label-length="4" field-length="8">
					<s:textfield cssClass="form-control"
								requiredLabel="true"
								key="form.label.adr.cp"
								name="entrep.adr.cp"	/>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group field-resize" label-length="3" field-length="9">
					<s:textfield cssClass="form-control"
								requiredLabel="true"
								key="form.label.adr.pays"
								name="entrep.adr.pays" />
				</div>
			</div>
		</div>
		
		<div class="row form-btn-pad">
			<div class="col-lg-2 col-lg-offset-5" >
				<s:submit 	key				= "%{#submitButton}"
							validate		= "true"
							cssClass		= "btn btn-primary btn-sm btn-block"
							validateFunction= "bootstrapValidation"
							formIds			= "validationForm"		
				/>
			</div>
		</div>
	
		</s:form>
	</div>
	
</div>