<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.entreprise.show" /></b></h3>
	</div>
	
	<div class="panel-body">
		
		<div class="row">
		
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.raisonsocial"/></dt>
					<dd><s:property value="entrep.raisonSociale"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.siret"/></dt>
					<dd><s:property value="entrep.siret"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.rcs"/></dt>
					<dd><s:property value="entrep.rcs"/></dd>
				</dl>
			</div>
			
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.adr.title"/></dt>
					<dd><s:property value="entrep.adr.complement"/></dd>
					<dd><s:property value="entrep.adr.num"/>, <s:property value="entrep.adr.rue"/></dd>
					<dd><s:property value="entrep.adr.cp"/> <s:property value="entrep.adr.ville"/></dd>
					<dd><s:property value="entrep.adr.pays"/></dd>
				</dl>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-5" >
				<a href="<s:url action= 'entrepriseModify'><s:param name='idBean' value='idBean'/></s:url>" >
					<button type="button" class="btn btn-primary btn-sm btn-block">
						<s:text name="global.button.update"/>
					</button>
				</a>
			</div>
		</div>
	
	</div>
	
</div>