<%@ taglib prefix="s" uri="/struts-tags" %>

<s:if test="etudiant.id">
	<s:set name="action" value="'etudiantUpdate'"/>
	<s:set name="title" value="'page.admin.title.etudiant.update'"/>
	<s:set name="submitButton" value="%{getText('global.button.update')}"/>
	<s:set name="emailDisabled" value="true"/>
	<s:set name="email" value="etudiant.per.email"/>
	<s:set name="myIdBean" value="idBean"/>
</s:if>
<s:else>
	<s:set name="action" value="'etudiantSave'"/>
	<s:set name="title" value="'page.admin.title.etudiant.create'"/>
	<s:set name="submitButton" value="%{getText('global.button.create')}"/>
	<s:set name="emailDisabled" value="false"/>
</s:else>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="%{#title}" /></b></h3>
	</div>

	<div class="panel-body">
		
		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
					<s:actionerror/>
					<s:fielderror />
			</div>
		</s:if>
		
		<s:form 	action	= "%{#action}"							method	= "post"
					validate= "true" 								theme	= "bootstrap"
					cssClass= "form-horizontal default-form"		id		= "validationForm" 
		>
		
		<s:if test="etudiant.id">
			<s:hidden name="idBean" value="%{#myIdBean}" />
			<s:hidden name="etudiant.per.email" value="%{#email}" />
		</s:if>
		
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield	cssClass="form-control"
										requiredLabel="true"
										key="form.label.nom"
										name="etudiant.per.nom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.prenom" 
										name="etudiant.per.prenom" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control "
										requiredLabel="true"
										key="form.label.email"
										name="etudiant.per.email"
										disabled="%{#emailDisabled}"/>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										type="date"
										id="datepicker"
										key="form.label.dateN" 
										name="dateString"/>
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="false"
										key="form.label.tel"
										name="etudiant.per.tel" />
					</div>
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.ine"
										name="etudiant.ine" />
					</div>
				</div>
			</div>
			
			<hr/>
			
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:select 		cssClass="form-control"
										list="groupesList"
										key="Groupe" 
										name="grpId"	/>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="5" field-length="7">
						<s:textfield 	cssClass="form-control"
										requiredLabel="false"
										key="form.label.numEtu"
										name="etudiant.num" 	/>
					</div>
				</div>
			</div>
			
			<hr/>
			
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group field-resize" label-length="2" field-length="10">
						<s:textfield 	cssClass="form-control"
										requiredLabel="false"
										key="form.label.adr.complement"
										name="etudiant.adr.complement"
					/></div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="6" field-length="6">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.adr.num"
										name="etudiant.adr.num"
					/></div>
				</div>
				<div class="col-lg-8">
					<div class="form-group field-resize" label-length="2" field-length="10">
						<s:textfield 	cssClass="form-control"
							
										requiredLabel="true"
										key="form.label.adr.rue"
										name="etudiant.adr.rue" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="6" field-length="6">
						<s:textfield 	cssClass="form-control"
										requiredLabel="true"
										key="form.label.adr.ville"
										name="etudiant.adr.ville"/></div>
					</div>
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="4" field-length="8">
						<s:textfield cssClass="form-control"
									requiredLabel="true"
									key="form.label.adr.cp"
									name="etudiant.adr.cp"	/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="3" field-length="9">
						<s:textfield cssClass="form-control"
									requiredLabel="true"
									key="form.label.adr.pays"
									name="etudiant.adr.pays" />
					</div>
				</div>
			</div>
			
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<s:submit 	key				= "%{#submitButton}"
								validate		= "true"
								cssClass		= "btn btn-primary btn-sm btn-block"
								validateFunction= "bootstrapValidation"
								formIds			= "validationForm"	
					/>
				</div>
			</div>
			
		</s:form>
		
	</div>
	
</div>