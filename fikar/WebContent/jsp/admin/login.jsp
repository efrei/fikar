<%@ taglib prefix="s" uri="/struts-tags" %>


<script>

</script>

<s:if test="hasActionErrors()">
  		<div class="errors"><s:actionerror/></div>
</s:if>
 <s:form  
 	method="post" 				action="login.admin.execute" 
	theme	= "bootstrap"		cssClass= "form-horizontal default-form"	
	id		= "validationForm">
 		
 	<div class="form-group field-resize" label-length="4" field-length="8">
		<s:textfield name="login" label="Nom d'utilisateur" cssClass="form-control" />
	</div>
	
	<div class="form-group field-resize" label-length="4" field-length="8">
		<s:password name="pwd" label="Mot de passe" cssClass="form-control" />
	</div>
	<s:submit value="Se connecter" cssClass="btn btn-success col-lg-3 col-lg-offset-9" style="font-weight:bold;"/>
</s:form>