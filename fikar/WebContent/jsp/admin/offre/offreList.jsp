<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="panel panel-primary">
	
	<div class="panel-heading">
    	<h3 class="panel-title"><b><s:text name="page.rh.offre.list" /></b></h3>
	</div>
	
	<div class="panel-body">
	    <div class="table-responsive" style="overflow-x:scroll;">
	        <table  class="table table-striped table-bordered table-hover" >
				
				<thead> 
					<tr>
						<th><s:text name="form.label.ref" /></th>
						<th><s:text name="form.label.titre" /></th>
						<th><s:text name="form.label.dureeMin" /></th>
						<th><s:text name="form.label.email" /></th>
						<th>
							<div class='text-center'><span class="inverse glyphicon glyphicon-th"></span></div>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<s:iterator value="offreList">
						<tr>
							<td><s:property value="ref"/></td>
							<td><s:property value="titre"/></td>
							<td><s:property value="dureeMin"/>mois</td>
							<td><s:property value="mailRep"/></td>							
							<td> 
								<a  href="<s:url action= 'offreValidationShow'><s:param name='idBean' value='id'/></s:url> " >
									<span class="glyphicon glyphicon-search" ></span>
								</a>
							</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
		</div>
	</div>	
</div>