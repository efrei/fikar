<%@ taglib prefix="s" uri="/struts-tags"%>


<ul class="nav nav-sidebar">


	<br />
	<br />
	
	<li><a href="<s:url action= 'profileShow'	/>"> <s:text
				name="global.menu.etudiant.showprofile" /></a></li>
				
	<li><a href="<s:url action= 'etuMdp'	/>"> <s:text
				name="global.menu.rh.pass" /></a></li>
				
	<li><a href="<s:url action= 'rechercheOffreList'	/>"> <s:text
				name="global.menu.etudiant.rechercheoffre" /></a></li>
	
	<li><a href="<s:url action= 'conventionList'/>"> <s:text
				name="global.menu.etudiant.convention.list" /></a></li>		

	<li><a href="<s:url action= 'logOut'	/>"> <s:text
				name="global.menu.logOut" /></a></li>
				
	
</ul>