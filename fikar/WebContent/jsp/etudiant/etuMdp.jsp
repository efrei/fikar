<%@ taglib prefix="s" uri="/struts-tags"%>


<s:actionmessage theme="bootstrap" />

<div class="panel panel-primary">

	<div class="panel-heading">
		<h3 class="panel-title">
			<b>Changer Mot de Pass</b>
		</h3>
	</div>

	<div class="panel-body">

		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
				<s:actionerror />
				<s:fielderror />
			</div>
		</s:if>

		<s:form action="etuChangeMdp" method="post" 
			theme="bootstrap" cssClass="form-horizontal default-form"
			id="validationForm">

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="5"
						field-length="7">
						<s:password cssClass="form-control" requiredLabel="true"
							name="etudiant.usr.mdp" key="form.label.mdp" />
					</div>
				</div>
			</div>
			
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5">
					<s:submit key="global.button.update" validate="true"
						cssClass="btn btn-primary btn-sm btn-block"
						validateFunction="bootstrapValidation" formIds="validationForm" />
				</div>
			</div>
		</s:form>
	</div>
</div>
