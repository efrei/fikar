<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.admin.title.etudiant.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
		
			<div class="col-lg-6">
			
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="etudiant.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="etudiant.per.prenom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="etudiant.per.email"/></dd>
				</dl>
				
			</div>
			
			<div class="col-lg-6">
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.dateN"/></dt>
					<dd><s:property value="dateString" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="etudiant.per.tel" default="Non renseigné"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.ine"/></dt>
					<dd><s:property value="etudiant.ine" default="Non renseigné"/></dd>
				</dl>
				
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.grp"/></dt>
					<dd><s:property value="etudiant.grp.nom"/></dd>
				</dl>
			</div>
			
			<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.numEtu"/></dt>
					<dd><s:property value="etudiant.num" default="Non renseigné"/></dd>
				</dl>
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-12">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.adr.title"/></dt>
					<dd><s:property value="etudiant.adr.complement"/></dd>
					<dd><s:property value="etudiant.adr.num"/>, <s:property value="etudiant.adr.rue"/></dd>
					<dd><s:property value="etudiant.adr.cp"/> <s:property value="etudiant.adr.ville"/></dd>
					<dd><s:property value="etudiant.adr.pays"/></dd>
				</dl>
			</div>
		</div>
	</div>
</div>
