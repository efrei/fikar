<%@ taglib prefix="s" 		uri="/struts-tags" %>
<%@ taglib prefix="sj" 		uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" 		uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="tiles" 	uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>

<html lang="fr">

	<head>
	    <title> EasyStage - Polytech Paris-Sud - </title>	
	    <meta charset="utf-8"/>
		<sj:head jqueryui="true"/>
	    <sb:head includeScripts="true" includeStyles="false" includeScriptsValidation="true"/>
	    <link href="<s:url value=	"/theme/bootstrap/css/bootstrap.min.css" />"	rel="stylesheet">
	    <link href="<s:url value=	"/theme/bootstrap/css/dashboard.css" />"	rel="stylesheet">	
		
	    <tiles:insertAttribute name="head" />	
	</head>	
	<body class="theme-body">
		<div class="container">
			        <div class="row">
			            <div class="col-lg-8 col-lg-offset-2">
			                <div class="theme-panel login-panel panel" style="border : 1px solid #fff;">
			                    <div class="panel-heading theme-panel-heading">
			                        <tiles:insertAttribute name="header" />
			                    </div>
			                    <div class="theme-panel-body panel-body">
			                    	<br/>
									<tiles:insertAttribute name="body" />
									<br/>			                    				                    
			                    </div>
			                    <div class="panel-footer theme-panel-footer">
									<tiles:insertAttribute name="footer" />			                    				                    
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    
			    <script type="text/javascript"> var onlyOneError = true ; </script>
			    
			    <script type="text/javascript" src= " <s:url value="/theme/bootstrap/js/easyStage.js"/> " >  </script>
			    
		</body>
</html>