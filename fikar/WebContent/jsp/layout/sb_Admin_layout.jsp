<%@ taglib prefix="s" 		uri="/struts-tags" %>
<%@ taglib prefix="sj" 		uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" 		uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="tiles" 	uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>

<html lang="fr">

	<head>
	    <title> EasyStage - Polytech Paris-Sud - </title>
	    <meta charset="utf-8"/>
		<sj:head jqueryui="true"/>
	    <sb:head includeScripts="true" includeStyles="false" includeScriptsValidation="true"/>
	    <%-- <link href="<s:url value=	"/theme/bootstrap/css/bootstrapAdmin.css" />"	rel="stylesheet"> --%>
		 
		<tiles:insertAttribute name="head" />
		<link href="<s:url value=	"/theme/bootstrap/css/app3_addon.css" />"	rel="stylesheet">
		
		
	</head>
	

	<body>
	
		<div style="min-height:100px;" class="navbar navbar-inverse navbar-fixed-top">
 	    	<div class="navbar-inner">
	        	
	        	<div  class="container">
					<tiles:insertAttribute name="header" />			
				</div>
				
 			</div>
		</div>
			
		<div style="margin-top:30px;" class="container-fluid">
			<div class="row">
					
					<div style="margin-top:30px;" class="col-sm-3 col-md-2 sidebar">
						<tiles:insertAttribute name="menu" />
					</div>
					
					<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
						<br />
						<tiles:insertAttribute name="body" />
					</div>
					
			</div>
		</div>	

	<script type="text/javascript" src= " <s:url value="/theme/bootstrap/js/easyStage.js"/> " >  </script>
	
	</body>
</html>

        