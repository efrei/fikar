<%@ taglib prefix="s" 		uri="/struts-tags" %>
<%@ taglib prefix="sj" 		uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" 		uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="tiles" 	uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>

<html lang="fr">

	<head>
	    <title> EasyStage - Polytech Paris-Sud - Espace Entrprise</title>
	    <meta charset="utf-8"/>
	    <sj:head jqueryui="true"/>
	    <sb:head includeScripts="true" includeStyles="false" includeScriptsValidation="true"/>
	    <link href="<s:url value=	"/theme/bootstrap/css/bootstrap.min.css" />"	rel="stylesheet">
	    <link href="<s:url value=	"/theme/bootstrap/css/dashboard.css" />"	rel="stylesheet">	
		
		<style type="text/css">
			h1.appTitle {
				text-align		: center;
				padding-top		: 10px;
				color			: #FFFFFF;
			}
			.form-pad-top	{
				padding-top		: 20px;
				/* border-top		: 1px solid #428bca; */
			}
			hr {
				display		: block;
				height		: 1px;
				border-top	: 1px solid #428bca;
				margin		: 15px 100px 30px 100px;
			}
			.form-btn-pad	{
				padding			: 15px 0px 0px 0px;
			}
			.hidden-button {
				visibility	: hidden;
			}
		</style>
		
	</head>
	

	<body>
	
		<div style="min-height:100px;" class="navbar navbar-inverse navbar-fixed-top">
 	    	<div class="navbar-inner">
	        	
	        	<div  class="container">
					<tiles:insertAttribute name="header" />			
				</div>
				
 			</div>
		</div>
			
		<div style="margin-top:30px;" class="container-fluid">
			<div class="row">
					
					<div style="margin-top:30px;" class="col-sm-3 col-md-2 sidebar">
						<tiles:insertAttribute name="menu" />
					</div>
					
					<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
						<br />
						<tiles:insertAttribute name="body" />
					</div>
					
			</div>
		</div>	


	<script>
	
			$(".field-resize").each(function(){
				var labelLength = $(this).attr("label-length");
				var fieldLength = $(this).attr("field-length");
				$("label", this).addClass("col-lg-"+labelLength);
				$(".controls", this).addClass("col-lg-"+fieldLength);
			});
	
		// Script permettant l'affichqge des erreurs  
			$(".alert-error").addClass("alert-danger");
				
		// Script for Errorfields 
			$("div.error").addClass("has-error");
			$(".form-group span.help-inline").remove();
			$("ul.errorMessage").addClass("row");
			$("ul.errorMessage li").addClass("col-lg-5 col-lg-offset-1");
	</script>

	</body>
</html>

        