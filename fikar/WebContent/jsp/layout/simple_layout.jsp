<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Simple Layout Title</title>
		<link rel="stylesheet" href="/xStage/css/jquery-ui.css">
  		<script src="/xStage/js/jquery-1.10.2.js"></script>
  		<script src="/xStage/js/jquery-ui.js"></script>
		
	</head>

	<body>
		
		<div style="background-color:red;" >
			<tiles:insertAttribute  name="header"/>
		</div>
			
		<div style="background-color:lightgray;" >
			<tiles:insertAttribute  name="body"/>
		</div>
		
		<div style="background-color:blue;" >
			<tiles:insertAttribute  name="footer"/>
		</div> 
	
	</body>
	
</html>