<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>

<html lang="fr">

<style type="text/css">
h1.appTitle {
	text-align: center;
	padding-top: 10px;
	font-weight: bold;
	color: #FFFFFF;
}

body .container {
	width: 980px;
}

.form-pad-top {
	padding-top: 20px;
	/* border-top		: 1px solid #428bca; */
}

hr {
	display: block;
	height: 1px;
	border-top: 1px solid #428bca;
	margin: 15px 100px 30px 100px;
}

.form-btn-pad {
	padding: 15px 0px 0px 0px;
}

.hidden-button {
	visibility: hidden;
}

.im-centered {
	margin: auto;
	max-width: 80%;
}
</style>

<head>
<title>FIKAR - Find your car -</title>
<meta charset="utf-8" />
<sj:head jqueryui="true" />
<sb:head includeScripts="true" includeStyles="false"
	includeScriptsValidation="true" />
<link href="<s:url value=	"/theme/bootstrap/css/bootstrapAdmin.css" />"
	rel="stylesheet">
<link href="<s:url value=	"/theme/bootstrap/css/dashboardAdmin.css" />"
	rel="stylesheet">
</head>



<body>
	<div class="container container-lg-height">
		<div class="row">
			<div class="col-lg-12 ">
				<div class="jumbotron" style="height: 180px;">
					<h1 style="text-align: center">FIKAR</h1>
					<p style="text-align: center">Find Your Car!</p>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-lg-12">
				<div id="carousel-example-generic" class="carousel slide"
					style="width: 550px; height: 230px; margin: 0 auto;">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0"
							class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="theme/img/cuba-1.jpg" alt="slide 1" style="margin: 0 auto;">
							<div class="container">
								<div class="carousel-caption">slide 1</div>
							</div>
						</div>
						<div class="item">
							<img src="theme/img/cuba-1.jpg" alt="slide 2" style="margin: 0 auto;">
							<div class="carousel-caption">slide 2</div>
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic"
						data-slide="prev"> <span class="icon-prev"></span>
					</a> <a class="right carousel-control" href="#carousel-example-generic"
						data-slide="next"> <span class="icon-next"></span>
					</a>
				</div>
			</div>
		</div>

		<br/> 

		<div class="row">
			<div class="col-lg-12">
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<a href="<s:url action='login.etudiant'/>">
							<button type="button"
								class="btn btn-success btn-lg">
								<h3><b>Espace Particulier</b></h3>
							</button>
						</a>
					</div>
					<div class="btn-group">
						<a href="<s:url action='login.admin'/>">
							<button type="button"
								class="btn btn-primary btn-lg  ">
								<h3>
								<b>
									Espace Admin</span>
								</b>
								</h3>
							</button>
						</a>
					</div>
					<div class="btn-group">
						<a href="<s:url action='login.rh'/>">
							<button type="button"
								class="btn btn-danger btn-lg">
								<h3><b>Espace Professionel</b></h3>
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>


</html>