<%@ taglib prefix="s" uri="/struts-tags"%>

<ul class="nav nav-sidebar">
	
	<li><a href="<s:url action= 'rhShow'	/>"> <s:text
				name="global.menu.rh.view" /></a></li>
	
	<li><a href="<s:url action= 'rhMdp'	/>"> <s:text
				name="global.menu.rh.pass" /></a></li>					

	<li><a href="<s:url action= 'offreCreate'	/>"> <s:text
				name="global.menu.rh.offre.creation" /></a></li>

	<li><a href="<s:url action= 'offreListNonValidee'	/>"> <s:text
				name="global.menu.rh.offre.listNonValidee" /></a></li>
	
	<li><a href="<s:url action= 'offreListValidee'	/>"> <s:text
				name="global.menu.rh.offre.listValidee" /></a></li>
	
	<li><a href="<s:url action= 'offreListRefusee'	/>"> <s:text
				name="global.menu.rh.offre.listRefusee" /></a></li>
	
	<li><a href="<s:url action= 'offreListCloturee'	/>"> <s:text
				name="global.menu.rh.offre.listCloturee" /></a></li>							

	<li><a href="<s:url action= 'logOut'	/>"> <s:text
				name="global.menu.logOut" /></a></li>

</ul>