<%@ taglib prefix="s" uri="/struts-tags"%>


<s:actionmessage theme="bootstrap" />

<div class="panel panel-primary">

	<div class="panel-heading">
		<h3 class="panel-title">
			<b>Cr�ation Offre</b>
		</h3>
	</div>

	<div class="panel-body">

		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
				<s:actionerror />
				<s:fielderror />
			</div>
		</s:if>

		<s:form action="offreSave" method="post" validate="true"
			theme="bootstrap" cssClass="form-horizontal default-form"
			id="validationForm">

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="5"
						field-length="7">
						<s:textfield cssClass="form-control" requiredLabel="true"
							name="offre.titre" 
							key="form.label.titre" />
					</div>
					<div class="form-group field-resize" label-length="5"
						field-length="7">
						<s:textfield cssClass="form-control" requiredLabel="true" 
							name="offre.mailRep"
							key="form.label.mailRep" />
					</div>
					<div class="form-group field-resize" label-length="5"
						field-length="7">
						<s:select cssClass="form-control"
							list="#{'1':'1 mois', '2':'2 mois', '3':'3 mois', '4':'4 mois','6':'6 mois', '9':'9 mois'}"
							key="Dur�e Minimum du stage" name="offre.dureeMin" />
					</div>
				</div>
				<div class="form-group field-resize" label-length="6"
					field-length="6">

					<div class="col-lg-6">
						<div class="form-group field-resize" label-length="4"
							field-length="8">
							<s:textfield cssClass="form-control" requiredLabel="false"
								name="offre.ref" key="form.label.ref" />
						</div>
						<div class="form-group field-resize" label-length="4"
							field-length="8">
							<s:textfield cssClass="form-control" requiredLabel="true"
								name="dateString" key="form.label.dateDebut" />
						</div>
					</div>
				</div>
			</div>


			<hr />

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="5"
						field-length="7">
						<s:textarea cssClass="form-control" requiredLabel="false"
							name="offre.profil_Recherche" key="form.label.profilRecherche" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4"
						field-length="6">
						<s:textarea cssClass="form-control" requiredLabel="true"
							name="offre.descrip" key="form.label.descrip" />
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-lg-10">
					<div class="form-group field-resize" label-length="3"
						field-length="9">
						<s:textarea cssClass="form-control" name="offre.commentaire"
							key="form.label.commentaire" />
					</div>
				</div>
			</div>
			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5">
					<s:submit key="global.button.create" validate="true"
						cssClass="btn btn-primary btn-sm btn-block"
						validateFunction="bootstrapValidation" formIds="validationForm" />
				</div>
			</div>

		</s:form>
	</div>
</div>