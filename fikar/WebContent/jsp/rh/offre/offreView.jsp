<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="panel panel-primary">

	<div class="panel-heading">
		<h3 class="panel-title">
			<b><s:text name="page.offre.title" /></b>
		</h3>
	</div>
	<div class="panel-body">

		<div class="row">
			<div class="col-lg-6">

				<dl class="dl-horizontal">
					<dt><s:text name="form.label.titre" /></dt>
					<dd><s:property value="offre.titre" /></dd>
				</dl>

				<dl class="dl-horizontal">
					<dt><s:text name="form.label.mailRep" /></dt>
					<dd><s:property value="offre.mailRep" /></dd>
				</dl>
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.descrip" /></dt>
					<dd><s:property value="offre.descrip" /></dd>
				</dl>
				
			</div>


			<div class="col-lg-6">

				<dl class="dl-horizontal">
					<dt><s:text name="form.label.ref" /></dt>
					<dd><s:property value="offre.ref" /></dd>
				</dl>
				
			</div>

			<hr />

			<div class="row">
				<div class="col-lg-6">

				<dl class="dl-horizontal">
					<dt><s:text name="form.label.profilRecherche" /></dt>
					<dd><s:property value="offre.profil_Recherche" /></dd>
				</dl>
				</div>
				<div class="col-lg-6">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.dureeMin" /></dt>
					<dd><s:property value="offre.dureeMin" /></dd>
				</dl>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-lg-10">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.commentaire" /></dt>
					<dd><s:property value="offre.commentaire" /></dd>
				</dl>
				</div>
			</div>
			
			<div class="row">
			<div class="col-lg-2 col-lg-offset-5" >
				<a href="<s:url action= 'offreClose'><s:param name='idBean' value='idBean'/></s:url>" >
					<button type="button" class="btn btn-primary btn-sm btn-block">
						<s:text name="global.button.close"/>
					</button>
				</a>
			</div>
		</div>
		</div>
	</div>
</div>