<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="rh.id">
	<s:set name="action" value="'rhUpdate'"/>
	<s:set name="title" value="'page.rh.title.update'"/>
	<s:set name="submitButton" value="'Modifer'"/>
	<s:set name="myIdBean" value="idBean"/>
</s:if>
<s:else>
	<s:set name="action" value="'rhSave'"/>
	<s:set name="title" value="'page.rh.title.create'"/>
<s:set name="submitButton" value="'Cr�er'"/>
	<s:set name="emailDisabled" value="false"/>
</s:else>

<s:actionmessage theme="bootstrap" />

<div class="panel panel-primary">

	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="%{#title}" /></b></h3>
	</div>

	<div class="panel-body">

		<s:if test="hasActionErrors() || hasFieldErrors()">
			<div class="alert alert-error actionError alert-danger">
				<s:actionerror />
				<s:fielderror />
			</div>
		</s:if>

		<s:form action="%{#action}" method="post" validate="true"
			theme="bootstrap" cssClass="form-horizontal default-form"
			id="validationForm">
			

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.nom" name="rh.per.nom" />
					</div>
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.prenom" name="rh.per.prenom" />
					</div>
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.email" name="rh.per.email" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="false"
							key="form.label.tel" name="rh.per.tel" />
					</div>
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.raisonsocial" name="rh.entrep.raisonSociale" />
					</div>
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" key="form.label.siret"
							name="rh.entrep.siret" />
					</div>
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" key="form.label.rcs"
							requiredLabel="true" name="rh.entrep.rcs" />
					</div>
				</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="6"
						field-length="6">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.adr.num" name="rh.entrep.adr.num" />
					</div>
				</div>
				<div class="col-lg-8">
					<div class="form-group field-resize" label-length="2"
						field-length="10">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.adr.rue" name="rh.entrep.adr.rue" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group field-resize" label-length="2"
						field-length="10">
						<s:textfield cssClass="form-control" requiredLabel="false"
							key="form.label.adr.complement" name="rh.entrep.adr.complement" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="6"
						field-length="6">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.adr.ville" name="rh.entrep.adr.ville" />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="4"
						field-length="8">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.adr.cp" name="rh.entrep.adr.cp" />
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group field-resize" label-length="3"
						field-length="9">
						<s:textfield cssClass="form-control" requiredLabel="true"
							key="form.label.adr.pays" name="rh.entrep.adr.pays" />
					</div>
				</div>
			</div>

			<div class="row form-btn-pad">
				<div class="col-lg-2 col-lg-offset-5" >
					<s:submit 	key				= "%{#submitButton}"
								validate		= "true"
								cssClass		= "btn btn-primary btn-sm btn-block"
								validateFunction= "bootstrapValidation"
								formIds			= "validationForm"	
					/>
				</div>
			</div>

		</s:form>
	</div>
</div>