<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="panel panel-primary">
	
	<div class="panel-heading">
		<h3 class="panel-title"><b><s:text name="page.rh.title.show" /></b></h3>
	</div>

	<div class="panel-body">
	
		<div class="row">
		
			<div class="col-lg-6">
			
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.nom"/></dt>
					<dd><s:property value="rh.per.nom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.prenom"/></dt>
					<dd><s:property value="rh.per.prenom"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.email"/></dt>
					<dd><s:property value="rh.per.email"/></dd>
				</dl>
				
			</div>
			
			<div class="col-lg-6">
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.adr.title"/></dt>
					<dd><s:property value="rh.entrep.adr.complement"/></dd>
					<dd><s:property value="rh.entrep.adr.num"/>, <s:property value="rh.entrep.adr.rue"/></dd>
					<dd><s:property value="rh.entrep.adr.cp"/> <s:property value="rh.entrep.adr.ville"/></dd>
					<dd><s:property value="rh.entrep.adr.pays"/></dd>
				</dl>
				
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.tel"/></dt>
					<dd><s:property value="rh.per.tel" default="Non renseigné"/></dd>
				</dl>
				
				
			</div>
		
		</div>
		
		<div class="row">
		
			<div class="col-lg-12">
				<dl class="dl-horizontal">
					<dt><s:text name="form.label.raisonsocial"/></dt>
					<dd><s:property value="rh.entrep.raisonSociale"/></dd>
					<dt><s:text name="form.label.rcs"/></dt>
					<dd><s:property value="rh.entrep.rcs"/>
					<dt><s:text name="form.label.siret"/></dt>
					<dd><s:property value="rh.entrep.siret"/>
				</dl>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-lg-offset-5" >
				<a href="<s:url action= 'rhModify'><s:param name='idBean' value='idBean'/></s:url>" >
					<button type="button" class="btn btn-primary btn-sm btn-block">
						<s:text name="global.button.update"/>
					</button>
				</a>
			</div>
		</div>

	</div>
</div>