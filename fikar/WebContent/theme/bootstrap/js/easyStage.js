 
if (onlyOneError != true) var onlyOneError = false; 

$(".disable").attr("disabled",true);

$(".field-resize").each(function(){
	var labelLength = $(this).attr("label-length");
	var fieldLength = $(this).attr("field-length");
	$("label", this).addClass("col-lg-"+labelLength);
	$(".controls", this).addClass("col-lg-"+fieldLength);
});

// Script permettant l'affichqge des erreurs  
$(".alert-error").addClass("alert-danger");
$(".errors").addClass("alert-danger");

// Script for Errorfields 
$("div.error").addClass("has-error");

$(".form-group span.help-inline").remove();
$("ul.errorMessage").addClass("row");
if ( !onlyOneError )$("ul.errorMessage li").addClass("col-lg-5");
$("ul.errorMessage li").addClass("col-lg-offset-1");
for (var i = 1 ; i <= 12 ; i++){
	$(".col-lg-"+i).addClass("col-md-"+i).addClass("col-sm-"+i);
	$(".col-lg-offset-"+i).addClass("col-md-offset-"+i).addClass("col-sm-offset-"+i);
}
$("input.btn-block").addClass("col-md-3").addClass("col-md-offset-2");

