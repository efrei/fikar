-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 04, 2014 at 08:43 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fikar`
--
CREATE DATABASE IF NOT EXISTS `fikar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fikar`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_per` int(11) NOT NULL,
  `id_usr` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_per_UNIQUE` (`id_per`),
  UNIQUE KEY `id_usr_UNIQUE` (`id_usr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `id_per`, `id_usr`) VALUES
(1, 4, 4),
(2, 5, 5),
(3, 19, 15),
(4, 27, 23);

-- --------------------------------------------------------

--
-- Table structure for table `adresse`
--

CREATE TABLE IF NOT EXISTS `adresse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `complement` varchar(45) DEFAULT NULL,
  `num` varchar(5) NOT NULL,
  `rue` varchar(100) NOT NULL,
  `ville` varchar(45) NOT NULL,
  `cp` varchar(8) NOT NULL,
  `pays` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `adresse`
--

INSERT INTO `adresse` (`id`, `complement`, `num`, `rue`, `ville`, `cp`, `pays`) VALUES
(1, NULL, '12', 'Rue Briand Leroy', 'Paris', '75000', 'FRANCE'),
(2, 'BÃ¢t 3', '34', 'Avenue Leclerc', 'Orsay', '91400', 'FRANCE'),
(3, 'RÃ©sidence Alexandre Dumas', '19', 'Rue de la Pacatrie', 'Orsay', '91400', 'FRANCE'),
(4, NULL, '23', 'Boulevard Jean JaurÃ¨s', 'Arcuiel', '94110', 'FRANCE'),
(5, NULL, '78', 'Boulevard Magenta', 'Paris', '75000', 'FRANCE'),
(6, NULL, '45', 'Rue Argentine', 'Paris', '75000', 'FRANCE'),
(7, 'RÃ©sidence Joliot Curie', '23', 'Rue Robinson', 'Issy-Les-Moulineaux', '92130', 'FRANCE'),
(8, '', '30', 'Rue Charles Ce Gaulle', 'Orsay', '91400', 'FRANCE'),
(9, '', '21', 'Av. Charles De Gaulle', 'Chatou', '78400', 'FRANCE'),
(10, '', '21', 'Av. de la paix', 'Chatou', '78400', 'FRANCE'),
(13, '', '21', 'Av. de la paix', 'Chatou', '78400', 'FRANCE'),
(14, '', '1018', 'Av. Jean Ferrandi', 'Paris', '75000', 'FRANCE'),
(15, '', '6', 'Av. Republique', 'Paris', '75006', 'FRANCE'),
(16, '', '12', 'Rue Louise Michel', 'Paris', '75004', 'FRANCE'),
(17, 'RÂ�sidence Alexandre Dumas', '19', 'Rue de la Pacatrie', 'Orsay', '91400', 'FRANCE');

-- --------------------------------------------------------

--
-- Table structure for table `convention`
--

CREATE TABLE IF NOT EXISTS `convention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `descrip` varchar(500) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `dateD` date NOT NULL,
  `dateF` date NOT NULL,
  `id_etu` int(11) NOT NULL,
  `id_entrep` int(11) NOT NULL,
  `id_enseignant` int(11) NOT NULL,
  `id_offre` int(11) DEFAULT NULL,
  `NOM_MAITRE` varchar(25) NOT NULL,
  `PRENOM_MAITRE` varchar(25) NOT NULL,
  `MAIL_MAITRE` varchar(100) NOT NULL,
  `TEL_MAITRE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_offre` (`id_offre`),
  KEY `id_enseignant` (`id_enseignant`),
  KEY `id_entrep` (`id_entrep`),
  KEY `id_etu` (`id_etu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `convention`
--

INSERT INTO `convention` (`id`, `titre`, `descrip`, `dateD`, `dateF`, `id_etu`, `id_entrep`, `id_enseignant`, `id_offre`, `NOM_MAITRE`, `PRENOM_MAITRE`, `MAIL_MAITRE`, `TEL_MAITRE`) VALUES
(1, 'Convention Stage SAP ', 'a very simple internship in SAP', '2014-06-05', '2015-01-09', 12, 2, 1, NULL, 'Vince', 'Vaugh', 'VV@intership.com', '010265248');

-- --------------------------------------------------------

--
-- Table structure for table `enseignant`
--

CREATE TABLE IF NOT EXISTS `enseignant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matricule` varchar(25) DEFAULT NULL,
  `fonctions` varchar(300) DEFAULT NULL,
  `id_per` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_per_UNIQUE` (`id_per`),
  UNIQUE KEY `matricule_UNIQUE` (`matricule`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `enseignant`
--

INSERT INTO `enseignant` (`id`, `matricule`, `fonctions`, `id_per`) VALUES
(1, 'EDP2110543', 'Coordinateur L3 Informatique;Professeur compilation Polytech Paris-Sud', 13),
(2, 'DDFR7748KLM', 'Professeur ModÃ©lisation - UML -', 14);

-- --------------------------------------------------------

--
-- Table structure for table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(100) NOT NULL,
  `rcs` varchar(25) NOT NULL,
  `siret` varchar(25) DEFAULT NULL,
  `id_adr` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `raison_sociale` (`raison_sociale`),
  UNIQUE KEY `rcs` (`rcs`),
  UNIQUE KEY `siret` (`siret`),
  KEY `id_adr` (`id_adr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `entreprise`
--

INSERT INTO `entreprise` (`id`, `raison_sociale`, `rcs`, `siret`, `id_adr`) VALUES
(1, 'easyStage', 'rcsEasyStage', 'siretEasyStage', 10),
(2, 'Google Cokkk', 'RcsGoogle', 'googleFRsiret', 9),
(3, 'newmannCO', 'vanNewman', 'siretVan', 15),
(4, 'SuperManCo', 'rcsSuper', 'eujjfl125', 16);

-- --------------------------------------------------------

--
-- Table structure for table `etudiant`
--

CREATE TABLE IF NOT EXISTS `etudiant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` varchar(20) DEFAULT NULL,
  `ine` varchar(20) DEFAULT NULL,
  `dateN` date DEFAULT NULL,
  `id_per` int(11) NOT NULL,
  `id_usr` int(11) NOT NULL,
  `id_adr` int(11) NOT NULL,
  `id_grp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_per_UNIQUE` (`id_per`),
  UNIQUE KEY `id_usr_UNIQUE` (`id_usr`),
  KEY `id_adr` (`id_adr`),
  KEY `id_grp` (`id_grp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `etudiant`
--

INSERT INTO `etudiant` (`id`, `num`, `ine`, `dateN`, `id_per`, `id_usr`, `id_adr`, `id_grp`) VALUES
(1, 'G3de0iJFHR', '21106134', '1992-10-09', 1, 1, 2, 1),
(2, '09iiOLz12er', '21106234', '1992-12-12', 2, 2, 17, 1),
(3, 'zTX09EIRYUT', '13398382', '1991-02-03', 3, 3, 2, 1),
(4, 'H98E0x0DI9Z', '21006453', '1990-10-10', 8, 8, 2, 2),
(5, 'XOJ98d782H1', '31107874', '1993-01-01', 10, 10, 6, 2),
(7, 'XsDEo90gBz2', '11198237', NULL, 12, 12, 6, 2),
(8, '', 'DDHK872KLM', '1992-12-12', 17, 13, 7, 2),
(9, '22054847', '012023005100', '1991-05-22', 18, 14, 8, 2),
(10, '', 'DDJ39482JD', NULL, 21, 16, 9, 2),
(11, '', 'DXJ34482JD', NULL, 22, 17, 10, 2),
(12, '', 'DXJ34482JC', '1992-06-11', 25, 20, 13, 1),
(13, '', 'e09ndnfvlk012', NULL, 26, 21, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groupe`
--

CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(65) COLLATE utf8_swedish_ci NOT NULL,
  `descrip` varchar(300) COLLATE utf8_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`, `descrip`) VALUES
(1, 'Apprentissage 3 - Informatique', '3Ã¨me annÃ©e du cycle d''ingÃ©nieur par apprentissage. SpÃ©cialitÃ© informatique.'),
(2, 'Polytech', 'Par dÃ©faut'),
(3, 'Apprentissage 4 - Informatique', '4Ã¨me annÃ©e du cycle d''ingÃ©nieur par apprentissage. SpÃ©cialitÃ© informatique');

-- --------------------------------------------------------

--
-- Table structure for table `maitre`
--

CREATE TABLE IF NOT EXISTS `maitre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_per` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_per` (`id_per`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `maitre`
--

INSERT INTO `maitre` (`id`, `id_per`) VALUES
(1, 26);

-- --------------------------------------------------------

--
-- Table structure for table `offre`
--

CREATE TABLE IF NOT EXISTS `offre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(30) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `titre` varchar(80) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `descrip` varchar(500) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `profil_recherche` varchar(500) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `dureeMin` int(11) NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `date_prop` date NOT NULL,
  `date_valid` date DEFAULT NULL,
  `date_cloture` date DEFAULT NULL,
  `est_validee` tinyint(1) NOT NULL DEFAULT '0',
  `est_cloturee` tinyint(1) NOT NULL DEFAULT '0',
  `mailRep` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `telRep` varchar(15) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `commentaire` varchar(300) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `id_maitre` int(11) DEFAULT NULL,
  `id_rh` int(11) NOT NULL,
  `id_entrep` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_maitre_2` (`id_maitre`),
  KEY `id_rh` (`id_rh`),
  KEY `id_entrep` (`id_entrep`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `offre`
--

INSERT INTO `offre` (`id`, `ref`, `titre`, `descrip`, `profil_recherche`, `dureeMin`, `dateDebut`, `date_prop`, `date_valid`, `date_cloture`, `est_validee`, `est_cloturee`, `mailRep`, `telRep`, `commentaire`, `id_maitre`, `id_rh`, `id_entrep`) VALUES
(7, '', 'Stage Web', 'Une stage de programmation web', 'etudiant en M1 debutant', 6, NULL, '2014-06-01', NULL, NULL, 1, 1, 'nsd@sd.com', NULL, 'jkdnfkjds', NULL, 2, 2),
(8, 'asd889', 'Google Offer', 'un stage commercial chez google', 'Ã©tudiant Bac + 12', 3, NULL, '2014-06-01', NULL, NULL, 1, 1, 'eh@d.com', NULL, 'jskdnfjksdf', NULL, 2, 2),
(9, '', 'Java development', 'une stage de developpement en java', 'un etudiant tres motive', 6, NULL, '2014-06-02', NULL, NULL, 1, 1, 'newrh@rh.com', NULL, '', NULL, 2, 2),
(10, '89kjkjg', 'analyse uml', 'stage analyse UML', 'un etudiant de M2 informatique', 6, NULL, '2014-06-02', NULL, NULL, 1, 1, 'newrh@rh.com', NULL, 'commenting', NULL, 2, 2),
(11, 'jk83jkff', 'Struts 2', 'Un stage de developpement applicatif web', 'un Ã©tudiant Bac +7 minimum', 1, NULL, '2014-06-02', NULL, NULL, 0, 1, 'kdjakjd@aksdj.com', NULL, 'asjdna', NULL, 2, 2),
(12, 'bnpJava89', 'BNP Java', 'un stage tres interessant', 'un etudiant', 4, NULL, '2014-06-02', NULL, NULL, 1, 1, 'kj@bnpparibas.net', NULL, '', NULL, 2, 2),
(13, 'googleOffre', 'Google', 'work at google', 'a genius', 9, NULL, '2014-06-02', NULL, NULL, 0, 1, 'staff@google.com', NULL, 'there will be alot of testing', NULL, 2, 2),
(14, 'saprh2947', 'Stage SAP', 'un stage', 'a student', 6, NULL, '2014-06-03', NULL, NULL, 1, 1, 'saprh@rh.fr', NULL, 'commenting', NULL, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `personne`
--

INSERT INTO `personne` (`id`, `nom`, `prenom`, `email`, `telephone`) VALUES
(1, 'Himri', 'Nabil', 'himnabil@gmail.com', '0652134857'),
(2, 'Kassem', 'Nelly', 'nellyk@live.com', NULL),
(3, 'Sarhan', 'Mohamed', 'sarhan.mohamed0@gmail.com', NULL),
(4, 'Dupont', 'Catherine', 'catherine.dupont@gmail.com', '0612131415'),
(5, 'Baudry', 'Florian', 'florian.baudry@yahoo.fr', '0741424344'),
(6, 'Patel', 'CÃ©cilia', 'cecilia.patel@yahoo.fr', '0621233443'),
(7, 'Duval', 'Enzo33', 'enzo.duval@free.fr', '0128363621'),
(8, 'Nault', 'CÃ©dric', 'cedric.nault@yahoo.fr', '0789864512'),
(9, 'Gauvin', 'Mathieu', 'mathieu.gauvin@gmail.com', '0654545454'),
(10, 'Semghouni', 'Younes', 'semghouni@gmail.com', NULL),
(12, 'Tifanny', 'Celia', 'celia.tifanny@yahoo.fr', '0690069911'),
(13, 'Pillac', 'David', 'david.pillac@live.fr', '0123233912'),
(14, 'Braham', 'Mohamed', 'm6momo@gmail.com', '0145879021'),
(17, 'Hanouna', 'Cyril', 'cyril.hanouna@yahoo.fr', '0652290943'),
(18, 'guillas', 'cedric', 'toto@yopmail.com', '0665655827'),
(19, 'Leberget', 'Robin', 'robin.ber@gmail.com', '0652134859'),
(21, 'Ba', 'Dioup', 'dioup.ba@yoube.com', ''),
(22, 'MacÂ�', 'Loic', 'loic.mace@u-pbud.fr', ''),
(25, 'Bousetta', 'Yazid', 'yaz.bousetta@emailer.com', ''),
(26, 'Nelly', 'Adam', 'nellya@live.com', ''),
(27, 'Smith', 'John', 'johnSmith@admin.com', '0102030405'),
(28, 'Smith', 'Jane', 'jane.smith@rh.com', ''),
(29, 'Robert', 'Smith', 'rj@rh.fr', ''),
(30, 'Gosling', 'James', 'msn@asd.fr', ''),
(31, 'Dean', 'James', 'sjfd@sjg.com', '0601020304'),
(32, 'Legend', 'John', 'tm@sdjf.com', '0601020304'),
(33, 'Rob', 'Alex', 'ar@rh.com', '0601062304'),
(36, 'Newmann', 'Van', 'vn@rh.com', '0203060504'),
(37, 'Kent', 'Clark', 'super@superman.fr', '0102030405');

-- --------------------------------------------------------

--
-- Table structure for table `rh`
--

CREATE TABLE IF NOT EXISTS `rh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_per` int(11) DEFAULT NULL,
  `id_usr` int(11) NOT NULL,
  `id_entrep` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_usr_UNIQUE` (`id_usr`),
  UNIQUE KEY `id_per_UNIQUE` (`id_per`),
  KEY `id_entrep` (`id_entrep`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `rh`
--

INSERT INTO `rh` (`id`, `id_per`, `id_usr`, `id_entrep`) VALUES
(1, 6, 6, 1),
(2, 7, 7, 2),
(4, NULL, 22, 2),
(5, 28, 24, NULL),
(6, 29, 25, NULL),
(7, 30, 26, NULL),
(8, 31, 27, NULL),
(9, 32, 28, NULL),
(10, 33, 29, NULL),
(11, 36, 32, 3),
(12, 37, 33, 4);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(15) NOT NULL,
  `mdp` varchar(63) NOT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `creator_index` (`creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `mdp`, `creator`) VALUES
(1, 'nhimri', 'newPASSword', 10),
(2, 'nkassem', 'etukassem', 10),
(3, 'msarhan', 'etusarhan', 10),
(4, 'cdupont ', 'admindupont', 1),
(5, 'fbaudry', 'adminbaudry', 10),
(6, 'cpatel', 'rhpatel', 1),
(7, 'eduval', '74455', 1),
(8, 'cnault', 'etunault', 1),
(9, 'mgauvin', 'rhgauvin', 1),
(10, 'ysemghouni', 'etusemghouni', 10),
(12, 'ctifanny', 'etutifanny', 10),
(13, 'CHanouna', 'etuHanouna', 4),
(14, 'cguillas', 'etuguillas', 4),
(15, 'Leberget.Robin', 'EasyForYou', 4),
(16, 'Ba.Dioup', 'EasyForYou', 4),
(17, 'MacÂ�.Loic', 'EasyForYou', 4),
(20, 'Yazid.Bousetta', 'EasyForYou', 4),
(21, 'John.Parker', 'EasyForYou', 5),
(22, 'James.Gosling', 'rh', 2),
(23, 'Smith.John', 'EasyForYou', 5),
(24, 'smith.jane', 'EasyForYou', 7),
(25, 'robby.junior', 'EasyForYou', 7),
(26, 'man.the', 'EasyForYou', 7),
(27, 'robr.bob', 'EasyForYou', 7),
(28, 'man.the2', 'EasyForYou', 7),
(29, 'rob.alex', 'EasyForYou', 7),
(32, 'Newmann.Van', 'EasyForYou', 7),
(33, 'Kent.Clark', 'EasyForYou', 7);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `personne` FOREIGN KEY (`id_per`) REFERENCES `personne` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `utilisateur` FOREIGN KEY (`id_usr`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `convention`
--
ALTER TABLE `convention`
  ADD CONSTRAINT `convention_ibfk_1` FOREIGN KEY (`id_etu`) REFERENCES `etudiant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `convention_ibfk_2` FOREIGN KEY (`id_entrep`) REFERENCES `entreprise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `convention_ibfk_3` FOREIGN KEY (`id_enseignant`) REFERENCES `enseignant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `convention_ibfk_5` FOREIGN KEY (`id_offre`) REFERENCES `offre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `enseignant`
--
ALTER TABLE `enseignant`
  ADD CONSTRAINT `enseignant_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `personne` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `entreprise_ibfk_1` FOREIGN KEY (`id_adr`) REFERENCES `adresse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `personne` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `etudiant_ibfk_2` FOREIGN KEY (`id_usr`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `etudiant_ibfk_3` FOREIGN KEY (`id_adr`) REFERENCES `adresse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `etudiant_ibfk_4` FOREIGN KEY (`id_grp`) REFERENCES `groupe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `maitre`
--
ALTER TABLE `maitre`
  ADD CONSTRAINT `maitre_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `personne` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `offre`
--
ALTER TABLE `offre`
  ADD CONSTRAINT `offre_ibfk_1` FOREIGN KEY (`id_maitre`) REFERENCES `maitre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `offre_ibfk_2` FOREIGN KEY (`id_rh`) REFERENCES `rh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `offre_ibfk_3` FOREIGN KEY (`id_entrep`) REFERENCES `entreprise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rh`
--
ALTER TABLE `rh`
  ADD CONSTRAINT `rh_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `personne` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `rh_ibfk_2` FOREIGN KEY (`id_usr`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `rh_ibfk_3` FOREIGN KEY (`id_entrep`) REFERENCES `entreprise` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
