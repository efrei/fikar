package com.fikar.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public enum MailAccount {
	FikarGmailAccount("no.reply.fikar@gmail.com", "m1aefrei", true, true,
			"smtp.gmail.com", 587);

	private MailAccount(final String username, final String password,
			boolean smtpAuth, boolean smtpStarttlsEnable, String smtpHost,
			int smtpPort) {
		this.username = username;
		this.password = password;

		this.mailerProperties = new Properties();
		this.mailerProperties.put("mail.smtp.auth", smtpAuth);
		this.mailerProperties.put("mail.smtp.starttls.enable",
				smtpStarttlsEnable);
		this.mailerProperties.put("mail.smtp.host", smtpHost);
		this.mailerProperties.put("mail.smtp.port", smtpPort);

		this.authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			};
		};
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Properties getMailerProperties() {
		return mailerProperties;
	}

	public Authenticator getAuthenticator() {
		return authenticator;
	}

	private String username;
	private String password;
	private Properties mailerProperties;
	private Authenticator authenticator;
}