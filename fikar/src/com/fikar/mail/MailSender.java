package com.fikar.mail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.fikar.model.bean.Personne;

public class MailSender {

	private Session session;
	public static boolean canSand = true;

	public MailSender(MailAccount account) {
		session = Session.getInstance(account.getMailerProperties(),
				account.getAuthenticator());
	}

	public void sendMailTo(Personne personne, String subject, String text) {
		if (canSand) {
			Message message = new MimeMessage(session);
			try {
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(personne.getEmail()));
				message.setSubject(subject);
				message.setText(text);
				Transport.send(message);

			} catch (MessagingException e) {
				// throw new RuntimeException(e);
				e.printStackTrace();
			}
		}
	}

}



