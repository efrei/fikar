package com.fikar.mail;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fikar.model.bean.Personne;

public class MailSenderTest {

	@Test
	public void Mailtest() {
		try {
			
			//Running the test should sent a mail to nellyk@live.com or throw an exception
			Personne p = new Personne();
			p.setEmail("nellyk@live.com");
			MailSender sender = new MailSender(MailAccount.FikarGmailAccount);
			sender.sendMailTo(p, "Fikar : Find your car", "This is a trial");
			// if everything goes well
			assertTrue(true);
		} catch (Exception e) {
			fail("Mail wasn't sent");
		}

	}
}
