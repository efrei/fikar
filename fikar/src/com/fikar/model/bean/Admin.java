package com.fikar.model.bean;

public class Admin {

	private Long id;
	private Personne per;
	private Utilisateur usr;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Personne getPer() {
		return per;
	}
	public void setPer(Personne per) {
		this.per = per;
	}
	public Utilisateur getUsr() {
		return usr;
	}
	public void setUsr(Utilisateur usr) {
		this.usr = usr;
	}
	
}
