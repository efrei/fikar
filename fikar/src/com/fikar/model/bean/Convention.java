package com.fikar.model.bean;

import java.util.Date;

@SuppressWarnings("serial")
public class Convention implements java.io.Serializable {
	
	private Long id;
	private String titre;
	private String descrip;
	private Date dateD;
	private Date dateF;
	
	private String nomMaitre;
	private String prenomMaitre;
	private String mailMaitre;
	private String telMaitre;
	
	private Etudiant etu;
	private Entreprise entrep;
	private Enseignant tuteur;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public Date getDateD() {
		return dateD;
	}
	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}
	public Date getDateF() {
		return dateF;
	}
	public void setDateF(Date dateF) {
		this.dateF = dateF;
	}
	public String getNomMaitre() {
		return nomMaitre;
	}
	public void setNomMaitre(String nomMaitre) {
		this.nomMaitre = nomMaitre;
	}
	public String getPrenomMaitre() {
		return prenomMaitre;
	}
	public void setPrenomMaitre(String prenomMaitre) {
		this.prenomMaitre = prenomMaitre;
	}
	public String getMailMaitre() {
		return mailMaitre;
	}
	public void setMailMaitre(String mailMaitre) {
		this.mailMaitre = mailMaitre;
	}
	public String getTelMaitre() {
		return telMaitre;
	}
	public void setTelMaitre(String telMaitre) {
		this.telMaitre = telMaitre;
	}
	public Etudiant getEtu() {
		return etu;
	}
	public void setEtu(Etudiant etu) {
		this.etu = etu;
	}
	public Entreprise getEntrep() {
		return entrep;
	}
	public void setEntrep(Entreprise entrep) {
		this.entrep = entrep;
	}
	public Enseignant getTuteur() {
		return tuteur;
	}
	public void setTuteur(Enseignant tuteur) {
		this.tuteur = tuteur;
	}
	
}
