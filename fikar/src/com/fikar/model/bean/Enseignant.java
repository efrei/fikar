package com.fikar.model.bean;

public class Enseignant {
	
	private Long id;
	private String matricule;
	private String fonctions;
	private Personne per;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public String getFonctions() {
		return fonctions;
	}
	public void setFonctions(String fonctions) {
		this.fonctions = fonctions;
	}
	public Personne getPer() {
		return per;
	}
	public void setPer(Personne per) {
		this.per = per;
	}
	
	
	
}
