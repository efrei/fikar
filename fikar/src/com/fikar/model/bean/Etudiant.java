package com.fikar.model.bean;

import java.util.Date;

public class Etudiant {
	
	private Long id;
	private String ine;
	private String num;
	private Date dateN;
	private Personne per;
	private Utilisateur usr;
	private Adresse adr;
	private Groupe grp;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIne() {
		return ine;
	}
	public void setIne(String ine) {
		this.ine = ine;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public Date getDateN() {
		return dateN;
	}
	public void setDateN(Date dateN) {
		this.dateN = dateN;
	}
	public Personne getPer() {
		return per;
	}
	public void setPer(Personne per) {
		this.per = per;
	}
	public Utilisateur getUsr() {
		return usr;
	}
	public void setUsr(Utilisateur usr) {
		this.usr = usr;
	}
	public Adresse getAdr() {
		return adr;
	}
	public void setAdr(Adresse adr) {
		this.adr = adr;
	}
	public Groupe getGrp() {
		return grp;
	}
	public void setGrp(Groupe grp) {
		this.grp = grp;
	}
	
}
