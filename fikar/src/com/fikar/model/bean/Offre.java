package com.fikar.model.bean;

import java.util.Date;

public class Offre {

	private Long id;
	private String ref;
	private String titre;
	private String descrip;
	private String profil_Recherche;
	private int dureeMin;
	private Date dateDebut;
	private Date date_prop;
	private Date date_valid;
	private Date date_cloture;
	private int estValidee;
	private int estCloturee;
	private String mailRep;
	private String telRep;
	private String commentaire;
	private Rh rh;
	private Entreprise entr;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public String getProfil_Recherche() {
		return profil_Recherche;
	}

	public void setProfil_Recherche(String profil_Recherche) {
		this.profil_Recherche = profil_Recherche;
	}

	public int getDureeMin() {
		return dureeMin;
	}

	public void setDureeMin(int dateMin) {
		this.dureeMin = dateMin;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDate_prop() {
		return date_prop;
	}

	public void setDate_prop(Date date_prop) {
		this.date_prop = date_prop;
	}

	public Date getDate_valid() {
		return date_valid;
	}

	public void setDate_valid(Date date_valid) {
		this.date_valid = date_valid;
	}

	public Date getDate_cloture() {
		return date_cloture;
	}

	public void setDate_cloture(Date date_cloture) {
		this.date_cloture = date_cloture;
	}

	public int getEstValidee() {
		return estValidee;
	}

	public void setEstValidee(int estValidee) {
		this.estValidee = estValidee;
	}

	public int getEstCloturee() {
		return estCloturee;
	}

	public void setEstCloturee(int estCloturee) {
		this.estCloturee = estCloturee;
	}

	public String getMailRep() {
		return mailRep;
	}

	public void setMailRep(String mailRep) {
		this.mailRep = mailRep;
	}

	public String getTelRep() {
		return telRep;
	}

	public void setTelRep(String telRep) {
		this.telRep = telRep;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Rh getRh() {
		return rh;
	}

	public void setRh(Rh rh) {
		this.rh = rh;
	}

	public Entreprise getEntr() {
		return entr;
	}

	public void setEntr(Entreprise entr) {
		this.entr = entr;
	}



	
}
