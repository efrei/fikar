package com.fikar.model.bean;

public class Rh {

	private Long id;
	private Personne per;
	private Utilisateur usr;
	private Entreprise entrep;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Personne getPer() {
		return per;
	}
	public void setPer(Personne per) {
		this.per = per;
	}
	public Utilisateur getUsr() {
		return usr;
	}
	public void setUsr(Utilisateur usr) {
		this.usr = usr;
	}
	public Entreprise getEntrep() {
		return entrep;
	}
	public void setEntrep(Entreprise entrep) {
		this.entrep = entrep;
	}
	
}
