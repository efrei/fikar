package com.fikar.model.bean;

@SuppressWarnings("serial")
public class Utilisateur implements java.io.Serializable {
	
	private Long id;
	private Utilisateur creator;
	private String login;
	private String mdp;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public Utilisateur getCreator() {
		return creator;
	}
	public void setCreator(Utilisateur creator) {
		this.creator = creator;
	}
	
}
