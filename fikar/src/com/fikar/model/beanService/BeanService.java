
package com.fikar.model.beanService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.fikar.model.HibernateListener;


public class BeanService <T> {
	
	protected Class<? extends T> clazz;
	protected SessionFactory hibernateSessionFactory = null;
	protected Session hibernateSession = null;
	protected String beanClass ;
	
	public BeanService ( Class<? extends T> typeClass ) {
		this.hibernateSessionFactory = ( SessionFactory ) ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME);
		clazz = typeClass ;
		beanClass = clazz.getSimpleName();
	}
	
	public void openSession(){
		this.hibernateSession = (( SessionFactory )ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME)).openSession();
	}
	
	public void closeSession(){
		if(this.hibernateSession != null){
		this.hibernateSession.close();
		this.hibernateSession = null;
	}}
	
	public void beginTransaction(){
		this.hibernateSession.beginTransaction();
	}
	
	public void commit(){
		this.hibernateSession.getTransaction().commit();
	}
	
	public void save ( T bean ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		this.beginTransaction();
		this.hibernateSession.save(bean);
		this.commit();
		this.closeSession();
	}
	
	public Map <String , String > cleanMapAttribute ( Map <String , String > attribute ){
		
		Map <String , String > result = new HashMap <String , String >();
		for (String attr : attribute.keySet()){
			String value = attribute.get(attr);
			if ( value != null && !value.equals(""))
				result.put(attr, value);
		}
		
		return result;
	}
	
	public Map <String , ClauseValue > cleanMapClauseValue ( Map <String , ClauseValue > attribute ){
		Map <String , ClauseValue > result = new HashMap <String , ClauseValue >();
		for (String attr : attribute.keySet()){
			ClauseValue value = attribute.get(attr);
			if ( value != null && value.getValue() != null && value.getClause() != null && !value.getValue().equals("") )
				result.put(attr, value);
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List <T> getBeanByAttribute ( Map <String , String > attribute ){
		
		if (attribute == null || attribute.isEmpty() ) return getAllBean();
		
		attribute = cleanMapAttribute( attribute );
		
		if ( attribute.isEmpty() ) return getAllBean();
		
		
		String queryText = "from "+ beanClass + " as b where " ;
		
		Iterator <String> it = attribute.keySet().iterator() ;
		if (it.hasNext()){
			String attr = it.next();
			queryText += "b."+ attr + " = '" + attribute.get(attr) + "' " ; 
		}
		while (it.hasNext()){
			String attr = it.next();
			queryText += "and b."+ attr + " = '" + attribute.get(attr) + "' " ;
		}
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (List <T>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List <T> getBeanByClauseValue ( Map <String , ClauseValue > attribute ){
		
		if (attribute == null || attribute.isEmpty() ) return getAllBean();
		attribute = cleanMapClauseValue(attribute);

		if ( attribute.isEmpty() ) return getAllBean();
		
		String queryText = "from "+ beanClass + " as b where" ;
		
		Iterator <String> it = attribute.keySet().iterator() ;
		if (it.hasNext()){
			String attr = it.next();
			queryText += " b."+ attr + attribute.get(attr)  ; 
		}
		while (it.hasNext()){
			String attr = it.next();
			queryText += " and b."+ attr + attribute.get(attr) ;
		}
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (List <T>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public T getUniqueBeanByClauseValue ( Map <String , ClauseValue > attribute ){
		
		if (attribute == null || attribute.isEmpty() ) return null;
		attribute = cleanMapClauseValue(attribute);

		if ( attribute.isEmpty() ) return null;
		
		String queryText = "from "+ beanClass + " as b where" ;
		
		Iterator <String> it = attribute.keySet().iterator() ;
		if (it.hasNext()){
			String attr = it.next();
			queryText += " b."+ attr + attribute.get(attr)  ; 
		}
		while (it.hasNext()){
			String attr = it.next();
			queryText += " and b."+ attr + attribute.get(attr) ;
		}
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (T) query.uniqueResult();
	}

	
	
	@SuppressWarnings("unchecked")
	public List <T> getBeanByAttribute ( String attribute , String value ){
		
		String queryText = "from "+ beanClass + " as b where b." + attribute + " = '" + value + "' " ;
		
				if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (List <T>) query.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public T getUniqueBeanByAttribute ( Map <String , String > attribute ){
		
		if (attribute == null || attribute.isEmpty() ) return null;
		attribute = cleanMapAttribute( attribute );
		
		if ( attribute.isEmpty() ) return null;
		
		String queryText = "from "+ beanClass + " as b where " ;
		
		Iterator <String> it = attribute.keySet().iterator() ;
		if (it.hasNext()){
			String attr = it.next();
			queryText += "b."+ attr + " = '" + attribute.get(attr) + "' " ; 
		}
		while (it.hasNext()){
			String attr = it.next();
			queryText += "and b."+ attr + " = '" + attribute.get(attr) + "' " ;
		}
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (T) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public T getUniqueBeanByAttribute ( String attribute , String value ){
		
		String queryText = "from "+ beanClass + " as b where b." + attribute + " = '" + value + "' " ;
		
				if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (T) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAllBean () {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		Query query  = this.hibernateSession.createQuery(" from "+ beanClass );
		
		return (List<T>) query.list();
	}
	
@SuppressWarnings("unchecked")
public List <T> getBeanByLikeAttribute ( Map <String , String > attribute ){
		
		if (attribute == null || attribute.isEmpty() ) return getAllBean();
		attribute = cleanMapAttribute( attribute );
		
		if ( attribute.isEmpty() ) return getAllBean();
		
		String queryText = "from "+ beanClass + " as b where " ;
		
		Iterator <String> it = attribute.keySet().iterator() ;
		if (it.hasNext()){
			String attr = it.next();
			queryText += "b."+ attr + " like '%" + attribute.get(attr) + "%' " ; 
		}
		while (it.hasNext()){
			String attr = it.next();
			queryText += "and b."+ attr + " like '%" + attribute.get(attr) + "%' " ;
		}
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return (List <T>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List <T> getBeanByLikeAttribute ( String attribute , String value ){
		
		String queryText = "from "+ beanClass + " as b where b." + attribute + " like '%" + value + "%' " ;
		
				if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		return (List <T>) query.list();
	}
}





