package com.fikar.model.beanService;


public class ClauseValue {
	
	private String value ;
	private Clause clause ;
	
	public ClauseValue (Clause clause , String Value){
		setClause(clause);
		setValue(Value);
	}
			
	@Override
	public
	String 
	toString(){
		return this.getClause().getExprésion(getValue());
	}
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Clause getClause() {
		return clause;
	}
	public void setClause(Clause clause) {
		this.clause = clause;
	}

	
}
