package com.fikar.model.service;

import org.hibernate.Query;

import com.fikar.model.bean.Admin;

public class AdminService extends ServiceSupport {

	private static AdminService instance = null;
	
	public AdminService() {
		super();
	}
	
	public static AdminService getInstance() {
		if ( AdminService.instance == null ) {
			AdminService.instance = new AdminService();
		}
		return AdminService.instance;
	}
	
	public Admin getAdminByLogin( String login ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Admin a = null;
		Query query  = this.hibernateSession.createQuery("from Admin as a where a.usr.login = :login");
		query.setString("login",login);
		a = (Admin) query.uniqueResult();
		
		return a;
	}
	
}
