package com.fikar.model.service;

import java.util.List;

import org.hibernate.Query;

import com.fikar.model.bean.Enseignant;

public class EnseignantService extends ServiceSupport {
	
	public EnseignantService() {
		super();
	}
	
	public Enseignant getEnseignantByMatricule( String matricule ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Enseignant e = null;
		Query query  = this.hibernateSession.createQuery("from Enseignant as e where e.matricule = :matricule");
		query.setString("matricule",matricule);
		e = (Enseignant) query.uniqueResult();
		
		return e;
	}

	@SuppressWarnings({ "unchecked" })
	public List<Enseignant> getEnseignantByName( String nom ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		List<Enseignant> s = null;
		Query query  = this.hibernateSession.createQuery("from Enseignant as e where e.per.nom = :nom");
		query.setString("nom",nom);
		s = (List<Enseignant>) query.list();
		return s;
	}
	
	public void save ( Enseignant e ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		this.beginTransaction();
		this.hibernateSession.save(e);
		this.commit();
		this.closeSession();
	}
	
}
