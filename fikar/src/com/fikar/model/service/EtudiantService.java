package com.fikar.model.service;

import java.util.List;

import org.hibernate.Query;

import com.fikar.model.bean.Etudiant;

public class EtudiantService extends ServiceSupport {
	
	public EtudiantService() {
		super();
	}
		
	public Etudiant getEtudiantByLogin( String login ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Etudiant e = null;
		Query query  = this.hibernateSession.createQuery("from Etudiant as e where e.usr.login = :login");
		query.setString("login",login);
		e = (Etudiant) query.uniqueResult();
		
		return e;
	}
	
	public Etudiant getEtudiantByINE( String ine ) {
		if (this.hibernateSession == null) {
			this.openSession();
		}
		Etudiant e = null;
		Query query  = this.hibernateSession.createQuery("from Etudiant as e where e.ine = :ine");
		query.setString("ine",ine);
		e = (Etudiant) query.uniqueResult();
		
		return e;
		
	}
	@SuppressWarnings("unchecked")
	public List<Etudiant> getEtudiantAll() {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		List<Etudiant> s = null;
		Query query  = this.hibernateSession.createQuery(" from Etudiant ");
		s = (List<Etudiant>) query.list();
		
		return s;
	}
	
	public void save ( Etudiant e ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		this.beginTransaction();
		this.hibernateSession.save(e);
		this.commit();
		this.closeSession();
	}

}