package com.fikar.model.service;

import java.util.List;

import org.hibernate.Query;

import com.fikar.model.bean.Offre;
import com.fikar.model.bean.Rh;

public class RhService extends ServiceSupport {
	private static RhService instance = null;

	public RhService() {
		super();
	}

	public Rh getRhByLogin(String login) {

		if (this.hibernateSession == null) {
			this.openSession();
		}

		Rh r = null;
		Query query = this.hibernateSession
				.createQuery("from Rh as r where r.usr.login = :login");
		query.setString("login", login);
		r = (Rh) query.uniqueResult();

		return r;
	}

	public static RhService getInstance() {
		if (RhService.instance == null) {
			RhService.instance = new RhService();
		}
		return RhService.instance;
	}

	@SuppressWarnings("unchecked")
	public List<Offre> getOffreAll() {

		if (this.hibernateSession == null) {
			this.openSession();
		}

		List<Offre> s = null;
		Query query = this.hibernateSession.createQuery(" from Offre ");
		s = (List<Offre>) query.list();

		return s;
	}

	public void save(Offre o) {

		if (this.hibernateSession == null) {
			this.openSession();
		}

		this.beginTransaction();
		this.hibernateSession.save(o);
		this.commit();
		this.closeSession();
	}

}