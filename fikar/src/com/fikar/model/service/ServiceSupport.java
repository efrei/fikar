package com.fikar.model.service;

import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.fikar.model.HibernateListener;
import com.fikar.model.bean.Etudiant;

public abstract class ServiceSupport {
	
	protected SessionFactory hibernateSessionFactory = null;
	protected Session hibernateSession = null;
	
	public ServiceSupport() {
		this.hibernateSessionFactory = ( SessionFactory ) ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME);
	}
	
	public void openSession(){
		this.hibernateSession = (( SessionFactory )ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME)).openSession();
	}
	
	public void closeSession(){
		this.hibernateSession.close();
		this.hibernateSession = null;
	}
	
	public void beginTransaction(){
		this.hibernateSession.beginTransaction();
	}
	
	public void commit(){
		this.hibernateSession.getTransaction().commit();
	}
	
	// cette fonction n'a pas encore �t� tester  
	public List getBeanByAttribute ( String beanClass , Map <String , String > attribute ){
		
		String queryText = "from "+ beanClass + " as b where " ;
		
		for (String attr : attribute.keySet() ){
			queryText += "b."+ attr + " = " + attribute.get(attr) + " " ; 
		}
		
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Query query  = this.hibernateSession.createQuery( queryText );
		
		return query.list();
	}
	
}

