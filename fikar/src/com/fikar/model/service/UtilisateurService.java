package com.fikar.model.service;

import org.hibernate.Query;

import com.fikar.model.bean.Etudiant;

public class UtilisateurService extends ServiceSupport{
	
	public UtilisateurService() {
		super();
	}
	
	public Etudiant getUtilisateurByLogin( String login ) {
		
		if (this.hibernateSession == null) {
			this.openSession();
		}
		
		Etudiant e = null;
		Query query  = this.hibernateSession.createQuery("from Utilisateur as u where u.login = :login");
		query.setString("login",login);
		e = (Etudiant) query.uniqueResult();
		
		return e;
	}

}
