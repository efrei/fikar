package com.fikar.web.admin.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.mail.MailAccount;
import com.fikar.mail.MailSender;
import com.fikar.model.bean.Admin;
import com.fikar.model.bean.Personne;
import com.fikar.model.bean.Utilisateur;
import com.fikar.model.beanService.BeanService;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;

@SuppressWarnings("serial")
public class AdminAction  extends DefaultAction {
	
	private MailSender sender ;
	
	public AdminAction (){
		sender = new MailSender(MailAccount.FikarGmailAccount);
	}
	
	private String idBean;
	private Admin admin;
	
	//For list (search)
		private List<Admin> adminList;
		private String nom;
		private String prenom;
	
	public String list() {
		BeanService<Admin> service = new BeanService<Admin>(Admin.class); 
		service.openSession();
		
		Map<String , String> map = new HashMap<String , String>();
		map.put( "per.nom", 	this.nom);
		map.put( "per.prenom", 	this.prenom);

		this.adminList = service.getBeanByLikeAttribute(map);
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String show() {
		if (this.idBean !=null) {
			BeanService<Admin> service = new BeanService<Admin>(Admin.class);
			service.openSession();
			this.admin = service.getUniqueBeanByAttribute("id", this.idBean );
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError( getText("action.error.admin.notFound") );
			return FAIL;
		}
	}
	
	public String update() {
		
		BeanService<Admin> serviceA = new BeanService<Admin>(Admin.class);
		serviceA.openSession();
		Admin adminToUpdate = serviceA.getUniqueBeanByAttribute("id", this.idBean );
		adminToUpdate.getPer().setNom(this.admin.getPer().getNom());
		adminToUpdate.getPer().setPrenom(this.admin.getPer().getPrenom());
		adminToUpdate.getPer().setTel(this.admin.getPer().getTel());
		
		if(adminToUpdate.getPer().getTel().length()<=0) adminToUpdate.getPer().setTel(null);
			
		serviceA.save(adminToUpdate);

		return SUCCESS;
	}
	
	public String save() {
		
		BeanService<Admin> serviceA = new BeanService<Admin>(Admin.class);
		BeanService<Personne> serviceP = new BeanService<Personne>(Personne.class);
		serviceA.openSession();
		serviceP.openSession();
		
		Admin adminFromDB = serviceA.getUniqueBeanByAttribute("per.email", admin.getPer().getEmail());
		if (adminFromDB == null){
			Personne p = serviceP.getUniqueBeanByAttribute("email",admin.getPer().getEmail());
			if (p != null){ // Email en base	
				if (p.getNom().equals( admin.getPer().getNom() ) && p.getPrenom().equals( admin.getPer().getPrenom() )){
					addActionMessage(getText("form.message.update.exist.personne"));
					if (!p.getTel().equals( admin.getPer().getTel() )){
						p.setTel( admin.getPer().getTel() );
						addActionMessage(getText("form.message.update.tel"));
					}
						admin.setPer(p);
						generateUsrInAdmin ();
						serviceA.save(admin);
						addActionMessage(getText("form.message.success.creation.admin"));
				}else{
					addActionError(getText("form.error.exist.email"));
				}
			}else{
				generateUsrInAdmin ();
				serviceA.save(admin);
				
				
				sender.sendMailTo(this.admin.getPer(), "Cr�eation de votre compte Admin EasyStage ", 
						"Bonjour, \n\nNous vous informons de la cre�aton de votre compte Admin EasyStage \n\n"
						+ "\t Login :"+ this.admin.getUsr().getLogin() + "\n"
						+ "\t Mot de passe :"+ this.admin.getUsr().getMdp()   + "\n\n\n"
								+ "Cordialement \n\n L'�quipe EasyStage");
				
				
				addActionMessage(getText("form.message.success.creation.admin"));
				return SUCCESS;
			}
		}else{
			addActionError(getText("form.error.exist.admin"));
		}
		//serviceA.closeSession();
		serviceP.closeSession();
		return INPUT;
	}
	
	private String generateLogin (){
		BeanService<Utilisateur> service = new BeanService<Utilisateur>(Utilisateur.class);
		String login = admin.getPer().getNom() + "." + admin.getPer().getPrenom() ;
		service.openSession();
		
		if (service.getUniqueBeanByAttribute("login", login) == null) {
			service.closeSession();
			return login ; 
		}
		
		int i = 2 ;
		
		while (service.getUniqueBeanByAttribute("login", login + i ) != null)
			i++;
		
		
		service.closeSession();
		return login + i ; 
	}
	
	private  void generateUsrInAdmin (){
		
		BeanService<Utilisateur> service = new BeanService<Utilisateur>(Utilisateur.class);
		admin.setUsr(new Utilisateur());
		
		admin.getUsr().setLogin(generateLogin());
		admin.getUsr().setMdp(getText("password.default"));
		
		Operator op = (Operator) ActionContext.getContext().getSession().get("operator");
		
		admin.getUsr().setCreator( service.getUniqueBeanByAttribute("login", op.getLogin()) );
		service.closeSession();
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public List<Admin> getAdminList() {
		return adminList;
	}

	public void setAdminList(List<Admin> adminList) {
		this.adminList = adminList;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}
}
