package com.fikar.web.admin.convention;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fikar.model.bean.Admin;
import com.fikar.model.bean.Convention;
import com.fikar.model.bean.Enseignant;
import com.fikar.model.bean.Entreprise;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Groupe;
import com.fikar.model.beanService.BeanService;
import com.fikar.web.common.DefaultAction;



@SuppressWarnings("serial")
public class ConventionAction extends DefaultAction{


	private Convention conv;
	private List<Convention> convList;
	private String idBean;
	
	private String idEtu;
	private String idEntrep;
	private String idEns;
	private String dateDString;
	private String dateFString;
	private HashMap<Long, String> groupesList;
	private String dateStringN;
	
	private String ine ;
	private String matricule ;
	private String rcs ;
	private String siret ;
	
	
	
	
	public String getDateDString() {
		return dateDString;
	}

	public void setDateDString(String dateDString) {
		this.dateDString = dateDString;
	}

	public String getDateFString() {
		return dateFString;
	}

	public void setDateFString(String dateFString) {
		this.dateFString = dateFString;
	}


	

	public ConventionAction() {
		//Loading Groupes for the dropDown
		BeanService<Groupe> serviceG =  new BeanService<Groupe>(Groupe.class);
		serviceG.openSession();
		List<Groupe> temp = (serviceG.getAllBean());
		this.groupesList = new HashMap<Long, String>();
		
		for (Groupe item: temp) {
			groupesList.put(item.getId(), item.getNom());
		}
		
		serviceG.closeSession();
	}

	//-------------------------------------------------------------------------

	public String list() {
		BeanService<Convention> service = new BeanService<Convention>(Convention.class); 
		service.openSession();
		Map<String , String> map = new HashMap<String , String>();
		map.put("etu.ine", 	this.ine);
		map.put("entrep.rcs", this.rcs);
		map.put("tuteur.matricule",this.matricule);
		map.put("entrep.siret", this.siret);
		
		this.convList = service.getBeanByLikeAttribute(map);
		
		service.closeSession();
		return SUCCESS;
}

	
	public String show() {
		BeanService<Convention> service = new BeanService<Convention>(Convention.class); 
		service.openSession();
		conv = service.getUniqueBeanByAttribute("id", this.idBean);
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		if (this.conv.getEtu().getDateN() != null)
			this.dateStringN = df.format((this.conv.getEtu().getDateN()));
		
		if (this.conv.getDateD() != null)
			this.dateDString = df.format((this.conv.getDateD()));
		
		if (this.conv.getDateF() != null)
			this.dateFString = df.format((this.conv.getEtu().getDateN()));
		service.closeSession();
		return SUCCESS;
	}
	
	public String update() {
		
		return save ();
	}
	
	public String save() {
		
		boolean canInsert = true;
		
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		try {
			this.conv.setDateD(df.parse(this.dateDString));
			this.conv.setDateF(df.parse(this.dateFString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		BeanService<Etudiant> serviceE = new BeanService<Etudiant>(Etudiant.class); 
		serviceE.openSession();
		Etudiant e = null;
		e = serviceE.getUniqueBeanByAttribute("id", this.idEtu);
		if (e == null) {
			addActionError("form.error.etudiant.notFound");
			canInsert = false;
		}
		this.conv.setEtu(e);
		serviceE.closeSession();
		
		BeanService<Entreprise> serviceEn = new BeanService<Entreprise>(Entreprise.class); 
		serviceEn.openSession();
		Entreprise en = null;
		en = serviceEn.getUniqueBeanByAttribute("id", this.idEntrep);
		if (e == null) {
			addActionError("form.error.entreprise.notFound");
			canInsert = false;
		}
		this.conv.setEntrep(en);
		serviceEn.closeSession();
		
		BeanService<Enseignant> serviceEns = new BeanService<Enseignant>(Enseignant.class); 
		serviceEns.openSession();
		Enseignant ens = null;
		ens = serviceEns.getUniqueBeanByAttribute("id", this.idEns);
		if (ens == null) {
			addActionError("form.error.enseignant.notFound");
			canInsert = false;
		}
		this.conv.setTuteur(ens);
		serviceEns.closeSession();
		if (canInsert){
			BeanService<Convention> serviceC = new BeanService<Convention>(Convention.class);
			serviceC.openSession();
			serviceC.save(this.conv);
		//	serviceC.closeSession();
		}
		return INPUT;
	}
	
	public String display() {
		return NONE;
	}

	
	//------------------------------------------------------------------------------------
	public List<Convention> getConvList() {
		return convList;
	}

	public Convention getConv() {
		return conv;
	}

	public void setConv(Convention conv) {
		this.conv = conv;
	}
	public void setConvList(List<Convention> convList) {
		this.convList = convList;
	}


	public String getIdBean() {
		return idBean;
	}


	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}


	public String getIdEtu() {
		return idEtu;
	}


	public void setIdEtu(String idEtu) {
		this.idEtu = idEtu;
	}


	public String getIdEntrep() {
		return idEntrep;
	}


	public void setIdEntrep(String idEntrep) {
		this.idEntrep = idEntrep;
	}


	public String getIdEns() {
		return idEns;
	}


	public void setIdEns(String idEns) {
		this.idEns = idEns;
	}


	public HashMap<Long, String> getGroupesList() {
		return groupesList;
	}


	public void setGroupesList(HashMap<Long, String> groupesList) {
		this.groupesList = groupesList;
	}

	public String getDateStringN() {
		return dateStringN;
	}

	public void setDateStringN(String dateStringN) {
		this.dateStringN = dateStringN;
	}
}