package com.fikar.web.admin.enseignant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fikar.model.bean.Enseignant;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.beanService.Clause;
import com.fikar.model.beanService.ClauseValue;
import com.fikar.web.common.DefaultAction;

@SuppressWarnings("serial")
public class EnseignantAction  extends DefaultAction {

	private Enseignant enseignant;
	private String grpId;
	private String idBean;
	
	private String actionForm;
	
	// For list (search)
		private List<Enseignant> ensList;
		private String nom;
		private String prenom;
		private String email;
		private String matricule;

	
	public String list() {	
		
		BeanService<Enseignant> service = new BeanService<Enseignant>(Enseignant.class); 
		service.openSession();
		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "per.nom", new ClauseValue (Clause.Like ,this.nom ));
		map.put( "per.prenom",  new ClauseValue (Clause.Like ,this.prenom ));
		map.put( "grp.id",  new ClauseValue (Clause.Equals ,this.grpId));
		map.put( "per.email", new ClauseValue (Clause.Like , this.email ));
		map.put( "matricule", new ClauseValue (Clause.Like , this.matricule ));
		this.ensList = service.getBeanByClauseValue(map);
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String show() {
		if (this.idBean !=null) {
			BeanService<Enseignant> service = new BeanService<Enseignant>(Enseignant.class);
			service.openSession();
			this.enseignant = service.getUniqueBeanByAttribute("id", this.idBean );
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError( getText("action.error.enseignant.notFound") );
			return FAIL;
		}
	}
	
	public String update() {
		
		BeanService<Enseignant> serviceE = new BeanService<Enseignant>(Enseignant.class);
		serviceE.openSession();
		
		Enseignant ensToUpdate = serviceE.getUniqueBeanByAttribute("id", idBean);
		
		ensToUpdate.getPer().setNom(enseignant.getPer().getNom());
		ensToUpdate.getPer().setPrenom(enseignant.getPer().getPrenom());
		ensToUpdate.getPer().setTel(enseignant.getPer().getTel());
		ensToUpdate.setFonctions(enseignant.getFonctions());
		
		if (enseignant.getPer().getTel().length() <= 0) ensToUpdate.getPer().setTel(null);
		
		boolean canUpdate = true ;
		Enseignant enseignantFromDB = null;
		
		if (enseignant.getMatricule().length() > 0 && !enseignant.getMatricule().equals(ensToUpdate.getMatricule()) ){
			
			enseignantFromDB = serviceE.getUniqueBeanByAttribute("matricule", enseignant.getMatricule());
			
			if (enseignantFromDB != null ){
				
				addActionError(getText("form.error.exist.matricule"));
				canUpdate = false ;
			}else
				ensToUpdate.setMatricule(enseignant.getMatricule());
		}
		if (canUpdate){
			serviceE.save(ensToUpdate);
			addActionMessage(getText("form.message.success.creation.enseignant"));
			return SUCCESS;
		}
		serviceE.closeSession();
		return INPUT;
	}
	
	public String save() {
		
		BeanService<Enseignant> serviceE = new BeanService<Enseignant>(Enseignant.class);
		serviceE.openSession();
		
		boolean canInsert = true;
		boolean enseignantExiste = false;
		
		Enseignant enseignantFromDB = null;
		enseignantFromDB = serviceE.getUniqueBeanByAttribute("per.email", enseignant.getPer().getEmail());
		
		if (enseignantFromDB != null){
			// Enseignant Existe
			if ( 	enseignantFromDB.getPer().getNom().equals(this.enseignant.getPer().getNom()) 
				&& 	enseignantFromDB.getPer().getPrenom().equals(this.enseignant.getPer().getPrenom())
				) {
				enseignantExiste = true;
				addActionError(getText("form.error.exist.enseignant"));
			}			
			else {
				addActionError(getText("form.error.exist.email"));
			}
			canInsert = false;
		}
		
		if (!enseignantExiste) {
			// INE existe 
			enseignantFromDB = null;
			enseignantFromDB = serviceE.getUniqueBeanByAttribute("matricule", enseignant.getMatricule());
			if (enseignantFromDB != null){
				addActionError(getText("form.error.exist.matricule"));
				canInsert = false ;
			}
		}
		
		if (canInsert){
			serviceE.save(enseignant);
			addActionMessage(getText("form.message.success.creation.enseignant"));
			return SUCCESS;
		}
		serviceE.closeSession();
		return INPUT;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}

	public String getActionForm() {
		return actionForm;
	}

	public void setActionForm(String actionForm) {
		this.actionForm = actionForm;
	}

	public List<Enseignant> getEnsList() {
		return ensList;
	}

	public void setEnsList(List<Enseignant> ensList) {
		this.ensList = ensList;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	
}
