package com.fikar.web.admin.entreprise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fikar.model.bean.Adresse;
import com.fikar.model.bean.Entreprise;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.beanService.Clause;
import com.fikar.model.beanService.ClauseValue;
import com.fikar.web.common.DefaultAction;

@SuppressWarnings("serial")
public class EntrepriseAction  extends DefaultAction{

	private Entreprise entrep;
	private String idBean;
	
	//For list (search)
		private List<Entreprise> entrepList;
		private String raisonSociale;
		private String rcs;
		private String siret;
	
	public String list() {
		BeanService<Entreprise> service = new BeanService<Entreprise>(Entreprise.class); 
		service.openSession();
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "raisonSociale", new ClauseValue (Clause.Like ,this.raisonSociale ));
		map.put( "rcs",  new ClauseValue (Clause.Like ,this.rcs ));
		map.put( "siret",  new ClauseValue (Clause.Like ,this.siret ));

		this.entrepList = service.getBeanByClauseValue(map);
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String show() {
		if (this.idBean !=null) {
			BeanService<Entreprise> service = new BeanService<Entreprise>(Entreprise.class);
			service.openSession();
			this.entrep = service.getUniqueBeanByAttribute("id", this.idBean );
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError( getText("action.error.entreprise.notFound") );
			return FAIL;
		}
	}
	
	public String update() {
		
		BeanService<Entreprise> serviceE = new BeanService<Entreprise>(Entreprise.class);
		serviceE.openSession();
		
		Entreprise entrepToUpdate = serviceE.getUniqueBeanByAttribute("id", idBean);
		
		boolean canUpdate = true;
		Entreprise e;
		
		e = null;
		e = serviceE.getUniqueBeanByAttribute("siret", this.entrep.getSiret());
		if ( e!= null && ! entrepToUpdate.getId().equals(e.getId()) ) {
			// Siret Existe D�j�
			addActionError("form.error.exist.siret");
			canUpdate = false;
		}
		
		e = null;
		e = serviceE.getUniqueBeanByAttribute("rcs", this.entrep.getRcs());
		if ( e!= null && ! entrepToUpdate.getId().equals(e.getId()) ) {
			// Rcs Existe D�j�
			addActionError("form.error.exist.rcs");
			canUpdate = false;
		}
		
		e = null;
		e = serviceE.getUniqueBeanByAttribute("raisonSociale", this.entrep.getRaisonSociale());
		if ( e!= null && ! entrepToUpdate.getId().equals(e.getId()) ) {
			// Raison Sociale Existe D�j�
			addActionError("form.error.exist.raisonSociale");
			canUpdate = false;
		}
		
		if (canUpdate) {
			
			//Updating RaisonSociale, Siret & Rcs
				entrepToUpdate.setRaisonSociale(this.entrep.getRaisonSociale());
				entrepToUpdate.setSiret(this.entrep.getSiret());
				entrepToUpdate.setRcs(this.entrep.getRcs());
			// ------
				
			// Adresse Update
				BeanService<Adresse> serviceAd = new BeanService<Adresse>(Adresse.class);
				Map <String , ClauseValue > adrAttr = new HashMap <String , ClauseValue >();
		
				adrAttr.put("complement", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getComplement()));
				adrAttr.put("num", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getNum()));
				adrAttr.put("rue", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getRue()));
				adrAttr.put("ville", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getVille()));
				adrAttr.put("cp", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getCp()));
				adrAttr.put("pays", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getPays()));
				
				Adresse adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
				if (adrFromDB != null && !adrFromDB.getId().equals( entrepToUpdate.getAdr().getId())) {
					// L'adresse existe d�j�
					entrepToUpdate.setAdr(adrFromDB);
				}	
				else if(adrFromDB == null) {
					// Nouvelle Adresse
					entrepToUpdate.setAdr(this.entrep.getAdr());
				}
		
				serviceAd.closeSession();
			// -----
				
				serviceE.save(entrepToUpdate);
				addActionMessage(getText("form.message.success.creation.etudiant"));
				return SUCCESS;
		}
	
		serviceE.closeSession();
		return INPUT;
	}
	
	public String save() {
		
		BeanService<Entreprise> serviceE = new BeanService<Entreprise>(Entreprise.class);
		serviceE.openSession();
		
		Boolean canInsert = true;
		
		Entreprise entrepFromDB = null;
		entrepFromDB = serviceE.getUniqueBeanByAttribute("rcs", this.entrep.getRcs());
		if (entrepFromDB!=null) {
			addActionError("form.error.exist.rcs");
			canInsert = false ;
		}
		
		entrepFromDB = null;
		entrepFromDB = serviceE.getUniqueBeanByAttribute("siret", this.entrep.getSiret());
		if (entrepFromDB!=null) {
			addActionError("form.error.exist.siret");
			canInsert = false ;
		}
		
		entrepFromDB = null;
		entrepFromDB = serviceE.getUniqueBeanByAttribute("raisonSociale", this.entrep.getRaisonSociale());
		if (entrepFromDB!=null) {
			addActionError("form.error.exist.raisonSociale");
			canInsert = false ;
		}
		
		BeanService<Adresse> serviceAd = new BeanService<Adresse>(Adresse.class);
		Map <String , ClauseValue > adrAttr = new HashMap <String , ClauseValue >();

		adrAttr.put("complement", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getComplement()));
		adrAttr.put("num", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getNum()));
		adrAttr.put("rue", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getRue()));
		adrAttr.put("ville", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getVille()));
		adrAttr.put("cp", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getCp()));
		adrAttr.put("pays", new ClauseValue(Clause.WithoutCase , this.entrep.getAdr().getPays()));
		
		Adresse adrFromDB = null;
		adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
		if (adrFromDB != null) this.entrep.setAdr(adrFromDB);
		
		if (canInsert){
			serviceE.save(this.entrep);
			addActionMessage(getText("form.message.success.creation.entreprise"));
			return SUCCESS;
		}
		System.out.println("here");
		serviceAd.closeSession();
		serviceE.closeSession();
		return INPUT;
	}
	
	public Entreprise getEntrep() {
		return entrep;
	}

	public void setEntrep(Entreprise entreprise) {
		this.entrep = entreprise;
	}

	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}

	public List<Entreprise> getEntrepList() {
		return entrepList;
	}

	public void setEntrepList(List<Entreprise> entreList) {
		this.entrepList = entreList;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getRcs() {
		return rcs;
	}

	public void setRcs(String rcs) {
		this.rcs = rcs;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

}
