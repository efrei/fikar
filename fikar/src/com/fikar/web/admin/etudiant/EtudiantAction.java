 package com.fikar.web.admin.etudiant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.mail.MailAccount;
import com.fikar.mail.MailSender;
import com.fikar.model.bean.Adresse;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Groupe;
import com.fikar.model.bean.Utilisateur;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.beanService.Clause;
import com.fikar.model.beanService.ClauseValue;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;

@SuppressWarnings("serial")
public class EtudiantAction  extends DefaultAction{
	
	private Etudiant etudiant;
	private String dateString;
	private String grpId;
	private Map<Long, String> groupesList;
	private String idBean;
	
	private String actionForm;
	
	// For list (search)
		private List<Etudiant> etuList;
		private String nom;
		private String prenom;
		private String email;
		private String ine;
		private String num;
		
	
	
	public EtudiantAction() {
		//Loading Groupes for the dropDown
			BeanService<Groupe> serviceG =  new BeanService<Groupe>(Groupe.class);
			serviceG.openSession();
			
			List<Groupe> temp = (serviceG.getAllBean());
			this.groupesList = new HashMap<Long, String>();
			
			for (Groupe item: temp) {
				groupesList.put(item.getId(), item.getNom());
			}
			
			serviceG.closeSession();		
	}
	
	public String list() {	
		
		BeanService<Etudiant> service = new BeanService<Etudiant>(Etudiant.class); 
		service.openSession();
		
		this.groupesList.put(null, "Tous");
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "per.nom", new ClauseValue (Clause.Like ,this.nom ));
		map.put( "per.prenom",  new ClauseValue (Clause.Like ,this.prenom ));
		map.put( "grp.id",  new ClauseValue (Clause.Equals ,this.grpId));
		map.put( "per.email", new ClauseValue (Clause.Like , this.email ));
		map.put( "ine", new ClauseValue (Clause.Like , this.ine ));
		map.put( "num",  new ClauseValue (Clause.Like ,this.num ));

		this.etuList = service.getBeanByClauseValue(map);
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String show() {
		if (this.idBean !=null) {
			BeanService<Etudiant> service = new BeanService<Etudiant>(Etudiant.class);
			service.openSession();
			this.etudiant = service.getUniqueBeanByAttribute("id", this.idBean );
			SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
			if (this.etudiant.getDateN() != null)this.dateString = df.format(this.etudiant.getDateN());
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError( getText("action.error.etudiant.notFound") );
			return FAIL;
		}
	}
	
	public String update() {
		
		if (this.dateString != null && this.dateString.length() >0 ){	
			SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
			try {
				this.etudiant.setDateN(df.parse(this.dateString));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else this.etudiant.setDateN(null);
		
		BeanService<Etudiant> serviceE = new BeanService<Etudiant>(Etudiant.class);
		serviceE.openSession();
		
		Etudiant etuToUpdate = serviceE.getUniqueBeanByAttribute("id", idBean);
		
		if (!etuToUpdate.getPer().getNom().equals(etudiant.getPer().getNom()))
			etuToUpdate.getPer().setNom(etudiant.getPer().getNom());
		
		if (!etuToUpdate.getPer().getPrenom().equals(etudiant.getPer().getPrenom()))
			etuToUpdate.getPer().setPrenom(etudiant.getPer().getPrenom());
		
		if (!etudiant.getPer().getTel().equals(etuToUpdate.getPer().getTel()))
			etuToUpdate.getPer().setTel(etudiant.getPer().getTel());
		if (etudiant.getPer().getTel().length() <= 0) etuToUpdate.getPer().setTel(null);
		
		if (!etuToUpdate.getDateN().equals(etudiant.getDateN()))
			etuToUpdate.setDateN(etudiant.getDateN());
			
		boolean canUpdate = true ;
		Etudiant etudiantFromDB = null;
		
		
		if (etudiant.getIne().length() > 0 && !etudiant.getIne().equals(etuToUpdate.getIne()) ){
			
			etudiantFromDB = serviceE.getUniqueBeanByAttribute("ine", etudiant.getIne());
			
			if (etudiantFromDB != null ){
				
				addActionError(getText("form.error.exist.ine"));
				canUpdate = false ;
			}else
				etuToUpdate.setIne(etudiant.getIne());
		}
		
		
		if(etudiant.getNum().length() > 0  && !etudiant.getNum().equals(etuToUpdate.getNum())){
			etudiantFromDB = null;
			etudiantFromDB = serviceE.getUniqueBeanByAttribute("num", etudiant.getNum());
			
			if (etudiantFromDB != null ){
				addActionError(getText("form.error.exist.num"));
				canUpdate = false ;
			}else etuToUpdate.setNum(etudiant.getNum());
		} if(etudiant.getNum().length() <= 0 ) etuToUpdate.setNum(null);
		
		if (Long.parseLong(grpId) != etuToUpdate.getGrp().getId().longValue() ){
			BeanService<Groupe> serviceG = new BeanService<Groupe>(Groupe.class);
			Groupe grp = serviceG.getUniqueBeanByAttribute("id", this.grpId);
			
			if (grp==null) {
				addActionError(getText("form.error.grp.incorrect"));
				canUpdate = false ;
			}
			else {
				etuToUpdate.setGrp(grp);
			}
			serviceG.closeSession();
		}
		BeanService<Adresse> serviceAd = new BeanService<Adresse>(Adresse.class);
		Map <String , ClauseValue > adrAttr = new HashMap <String , ClauseValue >();

		adrAttr.put("complement", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getComplement()));
		adrAttr.put("num", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getNum()));
		adrAttr.put("rue", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getRue()));
		adrAttr.put("ville", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getVille()));
		adrAttr.put("cp", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getCp()));
		adrAttr.put("pays", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getPays()));
		
		Adresse adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
		if (adrFromDB != null && !adrFromDB.getId().equals( etuToUpdate.getAdr().getId())) // un autre adr exsistante  
			etuToUpdate.setAdr(adrFromDB);
		
		else if(adrFromDB == null) // nouvelle adr
			etuToUpdate.setAdr(etudiant.getAdr());
		serviceAd.closeSession();
		
		
		if (canUpdate){
			//generateUsrInEtudiant();
			serviceE.save(etuToUpdate);
			addActionMessage(getText("form.message.success.creation.etudiant"));
			return SUCCESS;
		}
		serviceE.closeSession();
		return INPUT;
	}
	
	public String save() {
		
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		try {
			this.etudiant.setDateN(df.parse(this.dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		BeanService<Etudiant> serviceE = new BeanService<Etudiant>(Etudiant.class);
		serviceE.openSession();
		
		boolean canInsert = true;
		boolean etudiantExiste = false;
		
		Etudiant etudiantFromDB = null;
		etudiantFromDB = serviceE.getUniqueBeanByAttribute("per.email", etudiant.getPer().getEmail());
		
		if (etudiantFromDB != null){
			// Etudiant Existe
			if ( 	etudiantFromDB.getPer().getNom().equals(this.etudiant.getPer().getNom()) 
				&& 	etudiantFromDB.getPer().getPrenom().equals(this.etudiant.getPer().getPrenom())
				) {
				etudiantExiste = true;
				addActionError(getText("form.error.exist.etudiant"));
			}			
			// Email Existe
			else {
				addActionError(getText("form.error.exist.email"));
			}
			canInsert = false;
		}
		
		if (!etudiantExiste) {
			// INE existe 
			etudiantFromDB = null;
			etudiantFromDB = serviceE.getUniqueBeanByAttribute("ine", etudiant.getIne());
			if (etudiantFromDB != null){
				addActionError(getText("form.error.exist.ine"));
				canInsert = false ;
			}
			
			// Num Etu existe
			if (etudiant.getNum() != null &&  etudiant.getNum().length() != 0){
				etudiantFromDB = null;
				etudiantFromDB = serviceE.getUniqueBeanByAttribute("num", etudiant.getNum());
				if (etudiantFromDB != null){
					addActionError(getText("form.error.exist.num"));
					canInsert = false ;
				}
			}
		}
		
		BeanService<Groupe> serviceG = new BeanService<Groupe>(Groupe.class);
		Groupe grp = null;
		grp = serviceG.getUniqueBeanByAttribute("id", this.grpId);
		if (grp==null) {
			addActionError(getText("form.error.grp.incorrect"));
			canInsert = false ;
		}
		else {
			etudiant.setGrp(grp);
		}
		serviceG.closeSession();
		BeanService<Adresse> serviceAd = new BeanService<Adresse>(Adresse.class);
		Map <String , ClauseValue > adrAttr = new HashMap <String , ClauseValue >();

		adrAttr.put("complement", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getComplement()));
		adrAttr.put("num", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getNum()));
		adrAttr.put("rue", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getRue()));
		adrAttr.put("ville", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getVille()));
		adrAttr.put("cp", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getCp()));
		adrAttr.put("pays", new ClauseValue(Clause.WithoutCase , etudiant.getAdr().getPays()));
		
		Adresse adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
		if (adrFromDB != null) etudiant.setAdr(adrFromDB);
		
		if (canInsert){
			generateUsrInEtudiant();
			serviceE.save(etudiant);
			
			MailSender sender = new MailSender(MailAccount.FikarGmailAccount);
			sender.sendMailTo(this.etudiant.getPer(), "Cr�eation de votre compte Etudiant EasyStage ", 
					"Bonjour, \n\nNous vous informant de la cre�aton de votre compte Etudiant EasyStage \n\n"
					+ "\t        Login :"+ this.etudiant.getUsr().getLogin() + "\n"
					+ "\t Mot de passe :"+ this.etudiant.getUsr().getMdp()   + "\n\n\n"
							+ "Cordialement \n\n L'�quipe EasyStage");
			
			
			
			addActionMessage(getText("form.message.success.creation.etudiant"));
			return SUCCESS;
		}
		serviceE.closeSession();
		return INPUT;
	}
	
	private String generateLogin (){
		BeanService<Utilisateur> service = new BeanService<Utilisateur>(Utilisateur.class);
		String login = etudiant.getPer().getPrenom() + "." + etudiant.getPer().getNom();
		service.openSession();
		
		if (service.getUniqueBeanByAttribute("login", login) == null) {
			service.closeSession();
			return login ; 
		}
		
		int i = 2 ;
		
		while (service.getUniqueBeanByAttribute("login", login + i ) != null)
			i++;
			
		service.closeSession();
		return login + i ; 
	}

	private  void generateUsrInEtudiant (){
		
		BeanService<Utilisateur> service = new BeanService<Utilisateur>(Utilisateur.class);
		etudiant.setUsr(new Utilisateur());
		
		etudiant.getUsr().setLogin(generateLogin());
		etudiant.getUsr().setMdp(getText("password.default"));
		
		Operator op = (Operator) ActionContext.getContext().getSession().get("operator");
		
		etudiant.getUsr().setCreator( service.getUniqueBeanByAttribute("login", op.getLogin()) );
		service.closeSession();
	}

	
	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Map<Long, String> getGroupesList() {
		return groupesList;
	}

	public void setGroupesList(Map<Long, String> groupesList) {
		this.groupesList = groupesList;
	}

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}

	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}

	public List<Etudiant> getEtuList() {
		return etuList;
	}

	public void setEtuList(List<Etudiant> etuList) {
		this.etuList = etuList;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIne() {
		return ine;
	}

	public void setIne(String ine) {
		this.ine = ine;
	}

	public String getActionForm() {
		return actionForm;
	}

	public void setActionForm(String actionForm) {
		this.actionForm = actionForm;
	}
	
	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

}