package com.fikar.web.admin.offre;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fikar.model.bean.Offre;
import com.fikar.model.beanService.BeanService;
import com.fikar.web.common.DefaultAction;

@SuppressWarnings("serial")
public class OffreAction  extends DefaultAction {
	
	private Offre offre ;
	private String idBean;
	private List<Offre> offreList ;
	
	public String list(){
		Map<String , String> mapQuery = new HashMap<String,String>();
		mapQuery.put("estValidee", "0");
		mapQuery.put("estCloturee", "0");
		BeanService<Offre> service = new BeanService<Offre>( Offre.class);
		this.offreList = service.getBeanByAttribute(mapQuery);  
		service.closeSession();
		return SUCCESS ;
	}
	
	public String show(){
		BeanService<Offre> service = new BeanService<Offre>( Offre.class);
		this.offre = service.getUniqueBeanByAttribute("id", idBean );

		return SUCCESS ;
	}
	
	public String allow(){
		BeanService<Offre> service = new BeanService<Offre>( Offre.class);
		this.offre = service.getUniqueBeanByAttribute("id", idBean );
		this.offre.setEstValidee(1);
		this.offre.setEstCloturee(0);
		service.save(this.offre);
		return this.list() ;
	}
	
	public String deny(){
		BeanService<Offre> service = new BeanService<Offre>( Offre.class);
		this.offre = service.getUniqueBeanByAttribute("id", idBean );
		this.offre.setEstValidee(0);
		this.offre.setEstCloturee(1);
		service.save(this.offre);
		return this.list() ;
	}
	
	
	public List<Offre> getOffreList() {
		return offreList;
	}
	public void setOffreList(List<Offre> offreList) {
		this.offreList = offreList;
	}
	public String getIdBean() {
		return idBean;
	}
	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}
	public Offre getOffre() {
		return offre;
	}
	public void setOffre(Offre offre) {
		this.offre = offre;
	}
	
	
}
