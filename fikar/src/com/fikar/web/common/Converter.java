package com.fikar.web.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Converter {
	
	public static Date stringToDate(String dateInString, String dateFromat) {
		
		Date result = null;
		
		if ( Validator.validateDate(dateInString, dateFromat) ) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(dateFromat);
				result = formatter.parse(dateInString);
		 
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return result;
		
	}
}
