package com.fikar.web.common;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class DefaultAction extends ActionSupport {
	
	protected static final String FAIL 				= "fail";
	protected static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	protected static final String INDEX 				= "index";
	
	public String input() {
		return SUCCESS;
	}

}
