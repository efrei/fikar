package com.fikar.web.common;

import java.util.regex.Pattern;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Validator {
	
	public static boolean isNullOrEmpty ( String input ) {
		
		if ( input == null || input.length() == 0 ) {
			return true;
		}
		return false;
		
	}
	
	public static boolean validateEmail(String email_input) {
		
		String EMAIL_REGEX = 
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
		return Pattern.compile(EMAIL_REGEX).matcher(email_input).matches();
	}
	
	public static boolean validateTelephone (String phone_input) {
		
		// A completer
		
		return true;
	}
	
	public static boolean validateDate(String dateToValidate, String dateFromat) {
		
		if(dateToValidate == null){
			return false;
		}
 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
 
		try {
 
			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
 
		} catch (ParseException e) {
 
			e.printStackTrace();
			return false;
		}
 
		return true;
	}
	
	public static boolean validateAdresse (String complement, String num, String rue, String ville, String CP, String Pays) {
		
		// A completer
		
		return true;
	}

}
