package com.fikar.web.etudiant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.model.bean.Convention;
import com.fikar.model.bean.Enseignant;
import com.fikar.model.bean.Entreprise;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Groupe;
import com.fikar.model.beanService.BeanService;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;



@SuppressWarnings("serial")
public class ConventionAction extends DefaultAction{


	private Convention conv;
	private List<Convention> convList;
	private String idBean;
	
	private String idEtu;
	private String idEntrep;
	private String idEns;
	private String dateStringN;
	private String dateDString;
	private String dateFString;
	private HashMap<Long, String> groupesList;
	
	
//	private String ine ;
//	private String matricule ;
//	private String rcs ;
//	private String siret ;
//	
	
	
	public String getDateDString() {
		return dateDString;
	}

	public void setDateDString(String dateDString) {
		this.dateDString = dateDString;
	}

	public String getDateFString() {
		return dateFString;
	}

	public void setDateFString(String dateFString) {
		this.dateFString = dateFString;
	}


	
	

	public ConventionAction() {
		//Loading Groupes for the dropDown
		BeanService<Groupe> serviceG =  new BeanService<Groupe>(Groupe.class);
		serviceG.openSession();
		
		List<Groupe> temp = (serviceG.getAllBean());
		this.groupesList = new HashMap<Long, String>();
		
		for (Groupe item: temp) {
			groupesList.put(item.getId(), item.getNom());
		}
		
		serviceG.closeSession();
	}

	//-------------------------------------------------------------------------

	public String list() {
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		Operator op = (Operator) session.get("operator");
		System.out.println(op.getId().toString() + "'");
		BeanService<Convention> service = new BeanService<Convention>(Convention.class);
		this.convList = service.getBeanByAttribute("etu.id",op.getId().toString());
		return SUCCESS;
	}
	
	public String show() {
		BeanService<Convention> service = new BeanService<Convention>(Convention.class); 
		service.openSession();
		conv = service.getUniqueBeanByAttribute("id", this.idBean);
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		if (this.conv.getEtu().getDateN() != null)
			this.dateStringN = df.format((this.conv.getEtu().getDateN()));
		
		if (this.conv.getDateD() != null)
			this.dateDString = df.format((this.conv.getDateD()));
		
		if (this.conv.getDateF() != null)
			this.dateFString = df.format((this.conv.getEtu().getDateN()));
		service.closeSession();
		return SUCCESS;
	}
	
	public String display() {
		return NONE;
	}

	
	//------------------------------------------------------------------------------------
	public List<Convention> getConvList() {
		return convList;
	}

	public Convention getConv() {
		return conv;
	}

	public void setConv(Convention conv) {
		this.conv = conv;
	}
	public void setConvList(List<Convention> convList) {
		this.convList = convList;
	}


	public String getIdBean() {
		return idBean;
	}


	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}


	public String getIdEtu() {
		return idEtu;
	}


	public void setIdEtu(String idEtu) {
		this.idEtu = idEtu;
	}


	public String getIdEntrep() {
		return idEntrep;
	}


	public void setIdEntrep(String idEntrep) {
		this.idEntrep = idEntrep;
	}


	public String getIdEns() {
		return idEns;
	}


	public void setIdEns(String idEns) {
		this.idEns = idEns;
	}


	public HashMap<Long, String> getGroupesList() {
		return groupesList;
	}


	public void setGroupesList(HashMap<Long, String> groupesList) {
		this.groupesList = groupesList;
	}

//	public String getSiret() {
//		return siret;
//	}
//
//	public void setSiret(String siret) {
//		this.siret = siret;
//	}
//
//	public String getRcs() {
//		return rcs;
//	}
//
//	public void setRcs(String rcs) {
//		this.rcs = rcs;
//	}
//
//	public String getMatricule() {
//		return matricule;
//	}
//
//	public void setMatricule(String matricule) {
//		this.matricule = matricule;
//	}
//
//	public String getIne() {
//		return ine;
//	}
//
//	public void setIne(String ine) {
//		this.ine = ine;
//	}
}