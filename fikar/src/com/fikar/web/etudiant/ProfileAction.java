package com.fikar.web.etudiant;

import java.text.SimpleDateFormat;
import java.util.Date;






import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.model.bean.Adresse;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Groupe;
import com.fikar.model.bean.Personne;
import com.fikar.model.bean.Rh;
import com.fikar.model.bean.Utilisateur;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.service.EtudiantService;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;

@SuppressWarnings("serial")
public class ProfileAction extends DefaultAction {
	
	private Etudiant etudiant;
	
	@SuppressWarnings("unchecked")
	public String show()
	{
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");
		BeanService<Etudiant> service = new BeanService<Etudiant>(Etudiant.class);
		service.openSession();
		this.etudiant = service.getUniqueBeanByAttribute("id", op.getId().toString());
		System.out.println(this.etudiant.getPer().getEmail());
		return SUCCESS;
	}

	public String updatePass(){
		return SUCCESS;
	}
	
	
	public String changerPass(){
		Map<String, Object> session = ActionContext.getContext().getSession();
		
		if (session.get("operator") !=null) {
			BeanService<Etudiant> service = new BeanService<Etudiant>(Etudiant.class);
			service.openSession();
			Operator op = (Operator) session.get("operator");
			Etudiant etuFromDB = null;
			etuFromDB = service.getUniqueBeanByAttribute("id", op.getId().toString()); 		
			
			etuFromDB.getUsr().setMdp(this.etudiant.getUsr().getMdp());
			service.save(etuFromDB);
			addActionMessage(getText("form.message.success.modification.etudiant"));
			return SUCCESS;
			
		}
		else {
			addActionError(getText("action.error.etudiant.notFound") );
			return FAIL;
		}
	}
	
	
	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	
}
