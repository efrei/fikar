package com.fikar.web.etudiant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.model.bean.Offre;
import com.fikar.model.beanService.BeanService;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;


@SuppressWarnings("serial")
public class RechercheOffreAction extends DefaultAction {

	private List<Offre> offList;
	private String idBean;
	private Offre offre;
	
	

	public String list() {
		Map<String , String> mapQuery = new HashMap<String,String>();
		mapQuery.put("estValidee", "1");
		mapQuery.put("estCloturee", "0");
		BeanService<Offre> service = new BeanService<Offre>( Offre.class);
		this.offList = service.getBeanByAttribute(mapQuery);  
		service.closeSession();
		return SUCCESS ;
		
	}
	
	
	
	
	public String show() {
		
		if (this.idBean !=null) {
			BeanService<Offre> service = new BeanService<Offre>(Offre.class);
			service.openSession();
			this.offre = service.getUniqueBeanByAttribute("id", this.idBean );
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError(getText("action.error.offre.notFound") );
			return FAIL;
		}
	}


	public List<Offre> getOffList() {
		return offList;
	}

	public void setOffList(List<Offre> offList) {
		this.offList = offList;
	}


	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}
	

	public Offre getOffre() {
		return offre;
	}


	public void setOffre(Offre offre) {
		this.offre = offre;
	}
	
}
