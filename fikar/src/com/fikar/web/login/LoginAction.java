
package com.fikar.web.login;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.fikar.model.bean.Admin;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Rh;
import com.fikar.model.service.AdminService;
import com.fikar.model.service.EtudiantService;
import com.fikar.model.service.RhService;

@SuppressWarnings("serial")
public class LoginAction extends ActionSupport {
	
	private String login;
	private String pwd;
	
	public String input() {
		
		//=====================================
//			SessionFactory HibernateSessionFactory = (SessionFactory) ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME);
//			Session hibernateSession = HibernateSessionFactory.openSession();
//			
//			Personne p = new Personne();
//				p.setMail("celia.tifanny@yahoo.fr");
//				p.setNom("Tifanny");
//				p.setPrenom("Celia");
//				p.setTel("0690069911");
//			
//			Utilisateur u = new Utilisateur();
//				u.setLogin("ctifanny");
//				u.setMdp("etutifanny");
//				
//			Etudiant e = new Etudiant();
//				e.setIne("XsDEo90gBz2");
//				e.setNum("11198237");
//				e.setPer(p);
//				e.setUsr(u);
//			
//			System.out.println("Horatio Ol�r�");
//			hibernateSession.beginTransaction();
//			hibernateSession.save(e);
//			hibernateSession.getTransaction().commit();
//			
//			hibernateSession.close();
		
		
		//=====================================
		
		return SUCCESS;
	}
	
	public String home() {
		return SUCCESS;
	}
	
	public String loginEtudiant() {
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		EtudiantService etudiantService = new EtudiantService();
		
		etudiantService.openSession();
		
		Etudiant e = etudiantService.getEtudiantByLogin(this.login);
		
		if ( e != null && e.getUsr().getMdp().equals(this.pwd)) {
			Operator op = new Operator(e.getUsr().getLogin(), e.getPer().getId(), e.getUsr().getId(), e.getId(), "etudiant");
			session.put("operator", op);
			return SUCCESS;
		}
		else {
			addActionError("Login et/ou Mot de passe incorrecte(s) sur ce portail");
		}
		
		etudiantService.closeSession();
		return INPUT;
	}
	
	public String loginAdmin() {
		

		Map<String, Object> session = ActionContext.getContext().getSession();
		AdminService adminService = AdminService.getInstance();
		adminService.openSession();
		
		Admin a =  adminService.getAdminByLogin(this.login);//new Admin();
		/*/
			a.setUsr(new Utilisateur());
			a.getUsr().setLogin(login);
			a.getUsr().setMdp(pwd);
			a.setPer(new Personne());
			a.getPer().setNom("Coucou !!");
			a.getPer().setPrenom("Nabil s");
		/*/
				
		if ( a != null && a.getUsr().getMdp().equals(this.pwd)) {
			Operator op = new Operator(a.getUsr().getLogin(), a.getPer().getId(), a.getUsr().getId() ,a.getId(), "admin");
			session.put("operator", op);
			return SUCCESS;
		}
		else {
			addActionError("Login et/ou Mot de passe incorrecte(s) sur ce portail");
		}
		
		//adminService.closeSession();
		return INPUT;
	}

	public String loginRh() {
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		RhService rhService = new RhService();
		
		rhService.openSession();
		
		Rh r = rhService.getRhByLogin(this.login);
		
		if ( r != null && r.getUsr().getMdp().equals(this.pwd)) {
			Operator op = new Operator(r.getUsr().getLogin(), r.getPer().getId(), r.getUsr().getId(), r.getId(), "rh");
			session.put("operator", op);
			return SUCCESS;
		}
		else {
			addActionError("Login et/ou Mot de passe incorrecte(s) sur ce portail");
		}
		
		rhService.closeSession();
		return INPUT;
	}
	
	
	public String logOut (){
		Map<String, Object> session = ActionContext.getContext().getSession();
		Object op = session.get("operator");
		session.remove(op);
		session.put("operator", null);
		System.out.println(" ->>>>>>>>>>>>>>>>>>>> log out " + session.get("operator"));
		return INPUT;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
