package com.fikar.web.login;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

@SuppressWarnings("serial")
public class LoginInterceptor extends AbstractInterceptor {
	
	@Override
	public String intercept(ActionInvocation invocation)throws Exception{

		Map<String, Object> session = invocation.getInvocationContext().getSession();
		
		String nameSpace = invocation.getProxy().getNamespace().substring(1); //Substring to delete '/' at the beginning
		
		Operator op = (Operator) session.get("operator");
		
		if	( op != null ) {
			if ( op.getProfile().equals( nameSpace ) ) {
				return invocation.invoke();
			}
		}
		
		return "index";
	}
	
}