package com.fikar.web.login;

public class Operator {
	
	private String login;
	private Long idPer;
	private Long idUsr;
	private String profile;
	private Long id; //id de l'�tudiant, de l'admin ou de l'RH
	
	public Operator(String login, Long idPer, Long idUsr, Long id, String profile) {
		super();
		this.login = login;
		this.idPer = idPer;
		this.idUsr = idUsr;
		this.profile = profile;
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Long getIdPer() {
		return idPer;
	}
	public void setIdPer(Long idPer) {
		this.idPer = idPer;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdUsr() {
		return idUsr;
	}

	public void setIdUsr(Long idUsr) {
		this.idUsr = idUsr;
	}

}
