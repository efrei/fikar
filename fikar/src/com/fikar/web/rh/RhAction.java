package com.fikar.web.rh;

import java.util.HashMap;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.fikar.mail.MailAccount;
import com.fikar.mail.MailSender;
import com.fikar.model.bean.Adresse;
import com.fikar.model.bean.Entreprise;
import com.fikar.model.bean.Offre;
import com.fikar.model.bean.Personne;
import com.fikar.model.bean.Rh;
import com.fikar.model.bean.Utilisateur;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.beanService.Clause;
import com.fikar.model.beanService.ClauseValue;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;

@SuppressWarnings("serial")
public class RhAction extends DefaultAction {

	private Rh rh;
	boolean creatingFromLogin = false; 
	
	
	private String generateLogin() {
		BeanService<Utilisateur> service = new BeanService<Utilisateur>(
				Utilisateur.class);
		String login = rh.getPer().getNom() + "." + rh.getPer().getPrenom();
		service.openSession();

		if (service.getUniqueBeanByAttribute("login", login) == null) {
			service.closeSession();
			return login;
		}

		int i = 2;

		while (service.getUniqueBeanByAttribute("login", login + i) != null)
			i++;

		service.closeSession();
		return login + i;
	}

	private void generateUsrInRh() {

		BeanService<Utilisateur> service = new BeanService<Utilisateur>(
				Utilisateur.class);
		rh.setUsr(new Utilisateur());

		rh.getUsr().setLogin(generateLogin());
		rh.getUsr().setMdp(getText("password.default"));

		Operator op = (Operator) ActionContext.getContext().getSession()
				.get("operator");

		
		if (op != null )rh.getUsr().setCreator( service.getUniqueBeanByAttribute("login", op.getLogin()) );
		
		
		//service.closeSession();
		
		this.creatingFromLogin = (op == null) ;
	}

	public void setAsOperator(){
		if (this.creatingFromLogin){
			Map<String, Object> session = ActionContext.getContext().getSession();
			BeanService<Rh> rhService = new BeanService<Rh>(Rh.class);
			
			rhService.openSession();
			
			Rh r = rhService.getUniqueBeanByAttribute("per.email", this.rh.getPer().getEmail());
			
			Operator op = new Operator(r.getUsr().getLogin(), r.getPer().getId(), r.getUsr().getId(), r.getId(), "rh");
				session.put("operator", op);
			rhService.closeSession();
		}
		
	}
	
	public String execute() {
		BeanService<Rh> serviceA = new BeanService<Rh>(Rh.class);
		BeanService<Personne> serviceP = new BeanService<Personne>(
				Personne.class);
		serviceA.openSession();
		serviceP.openSession();

		Rh rhFromDB = serviceA.getUniqueBeanByAttribute("per.email", rh
				.getPer().getEmail());
		if (rhFromDB == null) {
			Personne p = serviceP.getUniqueBeanByAttribute("email", rh.getPer()
					.getEmail());
			if (p != null) { // si cet email est d�j� renseign� dans la base

				if (p.getNom().equals(rh.getPer().getNom())
						&& p.getPrenom().equals(rh.getPer().getPrenom())) {
					addActionMessage(getText("form.message.update.exist.personne"));
					if (!p.getTel().equals(rh.getPer().getTel())) {
						p.setTel(rh.getPer().getTel());
						addActionMessage(getText("form.message.update.tel"));
					}
					rh.setPer(p);
					generateUsrInRh();
					serviceA.save(this.rh);
					addActionMessage(getText("form.message.success.creation.rh"));

				} else {
					addActionError(getText("form.error.exist.email"));
				}
			} else {
				generateUsrInRh();
				serviceA.save(rh);
				addActionMessage(getText("form.message.success.creation.rh"));
			}
		} else {
			addActionError(getText("form.error.exist.rh"));
		}
		serviceA.closeSession();
		// serviceP.closeSession();
		return INPUT;// SUCCESS;
	}

	public String save() {

		BeanService<Rh> serviceR = new BeanService<Rh>(Rh.class);
		serviceR.openSession();

		boolean canInsert = true;

		Rh rhFromDB = null;
		rhFromDB = serviceR.getUniqueBeanByAttribute("per.email", rh.getPer()
				.getEmail());

		if (rhFromDB != null) {
			// rh Existe
			if (rhFromDB.getPer().getNom().equals(this.rh.getPer().getNom())
					&& rhFromDB.getPer().getPrenom()
							.equals(this.rh.getPer().getPrenom())) {
				addActionError(getText("form.error.exist.rh"));
			} else {
				addActionError(getText("form.error.exist.email"));
			}
			canInsert = false;
		}
		
		BeanService<Entreprise> serviceE = new BeanService<Entreprise>(Entreprise.class);
		serviceE.openSession();
			boolean entrepExist = false ;
		Entreprise entrepFromDB = null;
		entrepFromDB = serviceE.getUniqueBeanByAttribute("rcs", this.rh.getEntrep().getRcs());
		if (entrepFromDB != null  ) {
			if ( this.rh.getEntrep().getSiret()			.equals(entrepFromDB.getSiret()			)  
			&&   this.rh.getEntrep().getRaisonSociale()	.equals(entrepFromDB.getRaisonSociale()	) ){ // l'entreprise Exsiste d�j�
						// mettre a jour l'addresse //
				entrepExist = true; 
				
				BeanService<Adresse> serviceAd = new BeanService<Adresse>(
						Adresse.class);
				Map<String, ClauseValue> adrAttr = new HashMap<String, ClauseValue>();

				adrAttr.put("complement", new ClauseValue(Clause.WithoutCase,this.rh.getEntrep().getAdr().getComplement()));
				adrAttr.put("num", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getNum()));
				adrAttr.put("rue", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getRue()));
				adrAttr.put("ville", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getVille()));
				adrAttr.put("cp", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getCp()));
				adrAttr.put("pays", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getPays()));

				Adresse adrFromDB = null;
				adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
				if (adrFromDB != null)
					this.rh.getEntrep().setAdr(adrFromDB);
					
			}else{
				
				addActionError(getText("form.error.exist.rcs"));
				canInsert = false ;
			}
		}

		if (!entrepExist){
			entrepFromDB = null;
			entrepFromDB = serviceE.getUniqueBeanByAttribute("siret", this.rh.getEntrep().getSiret());
			if (entrepFromDB != null) {
				addActionError(getText("form.error.exist.siret"));
				canInsert = false ;
			}
	
			entrepFromDB = null;
			entrepFromDB = serviceE.getUniqueBeanByAttribute("raisonSociale",this.rh.getEntrep().getRaisonSociale());
			if (entrepFromDB != null) {
				addActionError(getText("form.error.exist.raisonSociale"));
				canInsert = false ;
			}
	
			BeanService<Adresse> serviceAd = new BeanService<Adresse>(
					Adresse.class);
			Map<String, ClauseValue> adrAttr = new HashMap<String, ClauseValue>();
	
			adrAttr.put("complement", new ClauseValue(Clause.WithoutCase,this.rh.getEntrep().getAdr().getComplement()));
			adrAttr.put("num", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getNum()));
			adrAttr.put("rue", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getRue()));
			adrAttr.put("ville", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getVille()));
			adrAttr.put("cp", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getCp()));
			adrAttr.put("pays", new ClauseValue(Clause.WithoutCase, this.rh.getEntrep().getAdr().getPays()));
	
			Adresse adrFromDB = null;
			adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
			if (adrFromDB != null)
				this.rh.getEntrep().setAdr(adrFromDB);
			serviceAd.closeSession();
		}
		if (canInsert) {
			this.generateUsrInRh();
			serviceR.save(this.rh);
			this.setAsOperator();
			
			MailSender sender = new MailSender(MailAccount.FikarGmailAccount);
			sender.sendMailTo(this.rh.getPer(), "Cr�eation de votre compte Rh EasyStage ", 
					"Bonjour, \n\nNous vous informant de la cre�aton de votre compte Rh EasyStage \n\n"
					+ "\t        Login :"+ this.rh.getUsr().getLogin() + "\n"
					+ "\t Mot de passe :"+ this.rh.getUsr().getMdp()   + "\n\n\n"
							+ "Cordialement \n\n L'�quipe EasyStage");
			
			addActionMessage(getText("form.message.success.creation.rh"));
			return SUCCESS;
		}
		serviceE.closeSession();
		serviceR.closeSession();
		return INPUT;
	}

	public String show(){
		Map<String, Object> session = ActionContext.getContext().getSession();
		
		if (session.get("operator") !=null) {
			BeanService<Rh> service = new BeanService<Rh>(Rh.class);
			service.openSession();
			Operator op = (Operator) session.get("operator");
			this.rh = service.getUniqueBeanByAttribute("id", op.getId().toString());
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError(getText("action.error.rh.notFound") );
			return SUCCESS;
		}
	}
	
	public String update(){
		
		BeanService<Rh> serviceR = new BeanService<Rh>(Rh.class);
		serviceR.openSession();
		//chercher le rh
		Map<String, Object> session = ActionContext.getContext().getSession();
		Operator op = (Operator) session.get("operator");
		this.rh.setId(op.getId());
		
		boolean canInsert = true;

		Rh rhFromDB = null;
		rhFromDB = serviceR.getUniqueBeanByAttribute("id", op.getId().toString());
				

		if (rhFromDB != null && !rhFromDB.getId().equals(this.rh.getId())) {
			// rh Existe
			if (rhFromDB.getPer().getNom().equals(this.rh.getPer().getNom())
					&& rhFromDB.getPer().getPrenom()
							.equals(this.rh.getPer().getPrenom())) {
				addActionError(getText("form.error.exist.rh"));
			} else {
				addActionError(getText("form.error.exist.email"));
			}
			canInsert = false;
		}
		
		BeanService<Entreprise> serviceE = new BeanService<Entreprise>(Entreprise.class);
		serviceE.openSession();
			boolean entrepExist = false ;
		Entreprise entrepFromDB = null;
		entrepFromDB = serviceE.getUniqueBeanByAttribute("rcs", rhFromDB.getEntrep().getRcs());
		if (entrepFromDB != null && !entrepFromDB.getId().equals(this.rh.getEntrep().getId())) {
			 // l'entreprise Existe d�j�
						// mettre a jour l'addresse //
				entrepExist = true; 
				
				BeanService<Adresse> serviceAd = new BeanService<Adresse>(
						Adresse.class);
				Map<String, ClauseValue> adrAttr = new HashMap<String, ClauseValue>();

				adrAttr.put("complement", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getComplement()));
				adrAttr.put("num", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getNum()));
				adrAttr.put("rue", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getRue()));
				adrAttr.put("ville", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getVille()));
				adrAttr.put("cp", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getCp()));
				adrAttr.put("pays", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getPays()));

				Adresse adrFromDB = null;
				adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
				if (adrFromDB != null && !adrFromDB.getId().equals(this.rh.getEntrep().getAdr().getId()))
					this.rh.getEntrep().setAdr(adrFromDB);
					
			}else{
				
				addActionError(getText("form.error.exist.rcs"));
				canInsert = false ;
			
		}

		if (!entrepExist){
			entrepFromDB = null;
			entrepFromDB = serviceE.getUniqueBeanByAttribute("siret", rhFromDB.getEntrep().getSiret());
			if (entrepFromDB != null && !entrepFromDB.getId().equals(rh.getEntrep().getId())) {
				addActionError(getText("form.error.exist.siret"));
				canInsert = false ;
			}
	
			entrepFromDB = null;
			entrepFromDB = serviceE.getUniqueBeanByAttribute("raisonSociale",rhFromDB.getEntrep().getRaisonSociale());
			if (entrepFromDB != null && !entrepFromDB.getId().equals( this.rh.getEntrep().getId())) {
				addActionError(getText("form.error.exist.raisonSociale"));
				canInsert = false ;
			}
	
			BeanService<Adresse> serviceAd = new BeanService<Adresse>(
					Adresse.class);
			Map<String, ClauseValue> adrAttr = new HashMap<String, ClauseValue>();
	
			adrAttr.put("complement", new ClauseValue(Clause.WithoutCase,rhFromDB.getEntrep().getAdr().getComplement()));
			adrAttr.put("num", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getNum()));
			adrAttr.put("rue", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getRue()));
			adrAttr.put("ville", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getVille()));
			adrAttr.put("cp", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getCp()));
			adrAttr.put("pays", new ClauseValue(Clause.WithoutCase, rhFromDB.getEntrep().getAdr().getPays()));
	
			Adresse adrFromDB = null;
			adrFromDB = serviceAd.getUniqueBeanByClauseValue(adrAttr);
			if (adrFromDB != null && !adrFromDB.getId().equals(this.rh.getEntrep().getAdr().getId()))
				this.rh.getEntrep().setAdr(adrFromDB);
			serviceAd.closeSession();
		}
		if (canInsert) {
			
			rhFromDB.getPer().setNom(this.rh.getPer().getNom());
			rhFromDB.getPer().setPrenom(this.rh.getPer().getPrenom());
			rhFromDB.getPer().setEmail(this.rh.getPer().getEmail());
			rhFromDB.getPer().setTel(this.rh.getPer().getTel());
			

			rhFromDB.getEntrep().getAdr().setComplement(this.rh.getEntrep().getAdr().getComplement());
			rhFromDB.getEntrep().getAdr().setCp(this.rh.getEntrep().getAdr().getCp());
			rhFromDB.getEntrep().getAdr().setVille(this.rh.getEntrep().getAdr().getVille());
			rhFromDB.getEntrep().getAdr().setPays(this.rh.getEntrep().getAdr().getPays());
			rhFromDB.getEntrep().getAdr().setRue(this.rh.getEntrep().getAdr().getRue());
			rhFromDB.getEntrep().getAdr().setNum(this.rh.getEntrep().getAdr().getNum());
			
			rhFromDB.getEntrep().setRaisonSociale(this.rh.getEntrep().getRaisonSociale());
			rhFromDB.getEntrep().setRcs(this.rh.getEntrep().getRcs());
			rhFromDB.getEntrep().setSiret(this.rh.getEntrep().getSiret());
			
			serviceR.save(rhFromDB);
			addActionMessage(getText("form.message.success.modification.rh"));
			return SUCCESS;
		}
		serviceE.closeSession();
		serviceR.closeSession();
		return INPUT;
		
	}
	
	public String updatePass(){
		return SUCCESS;
	}
	
	
	public String changerPass(){
		Map<String, Object> session = ActionContext.getContext().getSession();
		
		if (session.get("operator") !=null) {
			BeanService<Rh> service = new BeanService<Rh>(Rh.class);
			service.openSession();
			Operator op = (Operator) session.get("operator");
			Rh rhFromDB = null;
			rhFromDB = service.getUniqueBeanByAttribute("id", op.getId().toString()); 		
			
			rhFromDB.getUsr().setMdp(this.rh.getUsr().getMdp());
			service.save(rhFromDB);
			addActionMessage(getText("form.message.success.modification.rh"));
			return SUCCESS;
			
		}
		else {
			addActionError(getText("action.error.rh.notFound") );
			return FAIL;
		}
	}
	
	public Rh getRh() {
		return rh;
	}

	public void setRh(Rh rh) {
		this.rh = rh;
	}

}
