package com.fikar.web.rh.offre;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





import com.opensymphony.xwork2.ActionContext;
import com.fikar.model.bean.Etudiant;
import com.fikar.model.bean.Offre;
import com.fikar.model.bean.Rh;
import com.fikar.model.beanService.BeanService;
import com.fikar.model.beanService.Clause;
import com.fikar.model.beanService.ClauseValue;
import com.fikar.web.common.DefaultAction;
import com.fikar.web.login.Operator;

@SuppressWarnings("serial")
public class OffreAction extends DefaultAction {

	// For list (search)
		private List<Offre> offList;
		private String ref;
		private String titre;
		private Offre offre;
		private String idBean;
		private String pageTitle;

		
	public String list() {
		BeanService<Offre> service = new BeanService<Offre>(Offre.class); 
		service.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "ref", new ClauseValue (Clause.Like ,this.ref ));
		map.put( "titre",  new ClauseValue (Clause.Like ,this.titre ));
		map.put( "rh.id",  new ClauseValue (Clause.Equals ,op.getId().toString()));
		

		this.offList = service.getBeanByClauseValue(map);
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String save() {
		BeanService<Offre> serviceO = new BeanService<Offre>(Offre.class);
		serviceO.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		BeanService<Rh> serviceRh = new BeanService<Rh>(Rh.class);
		

		this.offre.setRh(serviceRh.getUniqueBeanByAttribute("id", op.getId().toString()));
		this.offre.setEntr(this.offre.getRh().getEntrep());
		
		Date start = new Date(System.currentTimeMillis());
		boolean canInsert = true;
		this.offre.setDate_prop(start);
		
		if (canInsert){
			this.offre.setEstCloturee(0);
			this.offre.setEstValidee(0);
			serviceO.save(this.offre);
			addActionMessage(getText("form.message.creation.success.offre"));
			return SUCCESS;
		}
		
		serviceO.closeSession();
		return INPUT;
	}

	public String show() {
		if (this.idBean !=null) {
			BeanService<Offre> service = new BeanService<Offre>(Offre.class);
			service.openSession();
			this.offre = service.getUniqueBeanByAttribute("id", this.idBean );
			service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError(getText("action.error.offre.notFound") );
			return FAIL;
		}
	}
	
	public String offreClose() {
		if (this.idBean !=null) {
			BeanService<Offre> service = new BeanService<Offre>(Offre.class);
			service.openSession();
			this.offre = service.getUniqueBeanByAttribute("id", this.idBean );
			this.offre.setEstCloturee(1);
			service.save(this.offre);
			//service.closeSession();
			return SUCCESS;
		}
		else {
			addActionError(getText("action.error.offre.notFound") );
			return FAIL;
		}
	}
	
	public String listValidee() {
		BeanService<Offre> service = new BeanService<Offre>(Offre.class); 
		service.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "ref", new ClauseValue (Clause.Like ,this.ref ));
		map.put( "titre",  new ClauseValue (Clause.Like ,this.titre ));
		map.put( "rh.id",  new ClauseValue (Clause.Equals ,op.getId().toString()));
		map.put( "estValidee",  new ClauseValue (Clause.Equals ,"1"));
		map.put( "estCloturee",  new ClauseValue (Clause.Equals ,"0"));
		

		this.offList = service.getBeanByClauseValue(map);
		this.pageTitle = getText("global.menu.rh.offre.listValidee");
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String listNonValidee() {
		BeanService<Offre> service = new BeanService<Offre>(Offre.class); 
		service.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "ref", new ClauseValue (Clause.Like ,this.ref ));
		map.put( "titre",  new ClauseValue (Clause.Like ,this.titre ));
		map.put( "rh.id",  new ClauseValue (Clause.Equals ,op.getId().toString()));
		map.put( "estValidee",  new ClauseValue (Clause.Equals ,"0"));
		map.put( "estCloturee",  new ClauseValue (Clause.Equals ,"0"));
		

		this.offList = service.getBeanByClauseValue(map);
		this.pageTitle = getText("global.menu.rh.offre.listNonValidee");
		
		service.closeSession();
		return SUCCESS;
	}
	
	public String listCloturee() {
		BeanService<Offre> service = new BeanService<Offre>(Offre.class); 
		service.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "ref", new ClauseValue (Clause.Like ,this.ref ));
		map.put( "titre",  new ClauseValue (Clause.Like ,this.titre ));
		map.put( "rh.id",  new ClauseValue (Clause.Equals ,op.getId().toString()));
		map.put( "estValidee",  new ClauseValue (Clause.Equals ,"1"));
		map.put( "estCloturee",  new ClauseValue (Clause.Equals ,"1"));

		this.offList = service.getBeanByClauseValue(map);
		this.pageTitle = getText("global.menu.rh.offre.listCloturee");
		service.closeSession();
		return SUCCESS;
	}
	
	public String listRefusee() {
		BeanService<Offre> service = new BeanService<Offre>(Offre.class); 
		service.openSession();
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Operator op = (Operator) session.get("operator");		
		
		Map<String , ClauseValue> map = new HashMap<String , ClauseValue>();
		map.put( "ref", new ClauseValue (Clause.Like ,this.ref ));
		map.put( "titre",  new ClauseValue (Clause.Like ,this.titre ));
		map.put( "rh.id",  new ClauseValue (Clause.Equals ,op.getId().toString()));
		map.put( "estValidee",  new ClauseValue (Clause.Equals ,"0"));
		map.put( "estCloturee",  new ClauseValue (Clause.Equals ,"1"));
		

		this.offList = service.getBeanByClauseValue(map);
		this.pageTitle = getText("global.menu.rh.offre.listRefusee");
		service.closeSession();
		return SUCCESS;
	}
	
	
	public List<Offre> getOffList() {
		return offList;
	}

	public void setOffList(List<Offre> offList) {
		this.offList = offList;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Offre getOffre() {
		return offre;
	}

	public void setOffre(Offre offre) {
		this.offre = offre;
	}
	public String getIdBean() {
		return idBean;
	}

	public void setIdBean(String idBean) {
		this.idBean = idBean;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	
}
